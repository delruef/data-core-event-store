data "aws_dynamodb_table" "event_store_table" {
  name = "ddb-${local.resources_arg_major_version}"
}

module "code_s3_bucket" {
    source = "../project-modules/s3_bucket"
    name = "code-${local.resources_arg}"
    tags = local.tags
}

module "payloads_s3_bucket" {
    source = "../project-modules/s3_bucket"
    name = "payloads-${local.resources_arg}"
    tags = local.tags
}

module "layer" {
    source = "../project-modules/layer"
    name = local.resources_arg
    bucket = module.code_s3_bucket.id
    folder = "layers"
}

module "proxy_api_gateway_lambda" {
    source = "../project-modules/proxy_api_gateway_lambda"
    name = local.resources_arg
    aws_region = var.aws_region
    aws_id = var.aws_id
    aws_env = var.aws_env
    code_bucket_id = module.code_s3_bucket.id
    payloads_bucket_arn = module.payloads_s3_bucket.arn
    dynamodb_resources = [
        data.aws_dynamodb_table.event_store_table.arn,
        "${data.aws_dynamodb_table.event_store_table.arn}/index/*"
    ]
    folder = "lambdas"
    layers = [module.layer.arn]
    lbd_var_env = {
        ENV = "production"
        DYNAMODB_TABLE_NAME = data.aws_dynamodb_table.event_store_table.id
        DYNAMODB_REGION = var.aws_region,
        LAMBDA_ROLES = join(",", local.lambda_target_roles)
        STORAGE_FILE_SYSTEM = "s3"
        STORAGE_PATH = module.payloads_s3_bucket.id
    }
    base_dns_domain = var.base_dns_domain
    version_name = var.version_name
    tags = local.tags
    lambda_target_roles = local.lambda_target_roles
}