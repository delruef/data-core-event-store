######################
#   GENERAL VAR
######################

variable "code_app" {
  description = "Code application"
}

variable "runtime" {
  description = "application ou core"
}

variable "code_soc" {
  description = "Code société"
}

variable "aws_id" {
  description = "ID du compte AWS"
}

variable "las_aws_id" {
  description = "ID du compte AWS"
}

variable "aws_account" {
  description = "Nom du compte AWS"
}

variable "aws_env" {
  description = "Nom environnement de travail"
}

variable "aws_region" {
  description = "Nom region"
}

variable "external_id" {
  description = "External ID"
}
variable "base_dns_domain" {
  description = "Nom DNS de base ie aws.terega.fr"
}

variable "version_name" {
  description = "Dashed name of the current version"
}
