locals {
  tags = {
    Application   = var.code_app
    Project       = var.code_app
    Billing       = var.aws_env
    Automated     = "true"
    ServiceLevel  = "AlwaysOn"
  }
  splitted_version_name = split("-", var.version_name)
  major_version_name = join("-", [local.splitted_version_name[0], local.splitted_version_name[1]])
  resources_arg_major_version = "${var.code_soc}-${var.runtime}-${var.code_app}-${var.aws_env}-${local.major_version_name}"
  resources_arg = "${var.code_soc}-${var.runtime}-${var.code_app}-${var.aws_env}-${var.version_name}"
  lambda_target_roles = [
    "arn:aws:iam::${var.aws_id}:role/rol-crossaccount-${var.aws_env}-api-interfaces",
    "arn:aws:iam::${var.aws_id}:role/rol-crossaccount-${var.aws_env}-pegaz",
    "arn:aws:iam::${var.aws_id}:role/rol-crossaccount-${var.aws_env}-dataex",
    "arn:aws:iam::${var.las_aws_id}:role/rol-crossaccount-las-${var.aws_env}-api-interfaces",
    "arn:aws:iam::${var.las_aws_id}:role/rol-crossaccount-las-${var.aws_env}-dataex",
  ]
}
