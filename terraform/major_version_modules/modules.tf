module "dynamodb" {
    source = "../project-modules/dynamodb"
    name = local.resources_arg
    tags = local.tags
}