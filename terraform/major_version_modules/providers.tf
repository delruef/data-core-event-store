terraform {
  required_version = "~>1.0.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>3.44.0"
    }
  }
  backend "s3" {
    dynamodb_table = "terega-terraform-lock"
    encrypt        = true
  }
}

provider "aws" {
  region = var.aws_region
  assume_role {
    role_arn = "arn:aws:iam::${var.aws_id}:role/rol-${var.code_soc}-${var.runtime}-${var.code_app}-${var.aws_account}-deploy"
    external_id = var.external_id
  }
}
