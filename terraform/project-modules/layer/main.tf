locals {
  file_name = "${var.folder}/${var.name}.zip"
}

resource "aws_lambda_layer_version" "lambda_layer" {
  layer_name    = "lyr-${var.name}"
  s3_bucket     = var.bucket
  s3_key        = local.file_name
  source_code_hash = base64sha256(local.file_name)
}
