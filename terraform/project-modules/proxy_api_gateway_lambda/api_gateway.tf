locals {
  apigw-name ="apigw-${var.name}"
}


resource "aws_api_gateway_domain_name" "domain_name" {
  count = var.base_dns_domain != "" ? 1 : 0
  regional_certificate_arn = aws_acm_certificate_validation.cert_validation[0].certificate_arn
  domain_name              = local.domain_name
  security_policy          = "TLS_1_2"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_base_path_mapping" "domain_mapping" {
  count = var.base_dns_domain != "" ? 1 : 0
  api_id      = aws_api_gateway_rest_api.api.id
  stage_name  = aws_api_gateway_stage.api-stage.stage_name
  domain_name = aws_api_gateway_domain_name.domain_name[count.index].domain_name
}

resource "aws_api_gateway_rest_api" "api" {
  name = local.apigw-name
  endpoint_configuration {
    types = var.endpoint_type
  }

}

resource "aws_lambda_permission" "apigw-execute" {
  statement_id            = "AllowAPIGatewayInvoke"
  action                  = "lambda:InvokeFunction"
  function_name           = aws_lambda_function.lambda_handler.function_name
  principal               = "apigateway.amazonaws.com"
  source_arn              = "${aws_api_gateway_rest_api.api.execution_arn}/*/*"
}


resource "aws_api_gateway_stage" "api-stage" {
  stage_name    = var.aws_env
  rest_api_id   = aws_api_gateway_rest_api.api.id
  deployment_id = aws_api_gateway_deployment.api-deployment.id
}


resource "aws_api_gateway_deployment" "api-deployment" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  depends_on  = [
    aws_api_gateway_integration.api-integration,
    aws_api_gateway_integration.apidoc-integration,
    aws_api_gateway_integration.swagger-integration
  ]
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_api_key" "api-key" {
  name        = "apik-${var.name}"
  description = "The API KEY to access to the ${local.apigw-name}"
}

resource "aws_api_gateway_usage_plan" "api-key-usage-plan" {
  name         = "usage-plan-${var.name}"

  api_stages {
    api_id = aws_api_gateway_rest_api.api.id
    stage  = aws_api_gateway_stage.api-stage.stage_name
  }
}

resource "aws_api_gateway_usage_plan_key" "main" {
  key_id        = aws_api_gateway_api_key.api-key.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.api-key-usage-plan.id

}

resource "aws_api_gateway_resource" "api-resource" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "{proxy+}"
}

resource "aws_api_gateway_resource" "apidoc-resource" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "openapi.json"
}

resource "aws_api_gateway_resource" "swagger-resource" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "docs"
}


resource "aws_api_gateway_method" "api-method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.api-resource.id
  http_method   = "ANY"
  authorization = "NONE"
  api_key_required = true
  request_parameters = {
    "method.request.path.proxy" = true
  }
}

resource "aws_api_gateway_method" "apidoc-method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.apidoc-resource.id
  http_method   = "GET"
  authorization = "NONE"
  api_key_required = false
  request_parameters = {
    "method.request.path.proxy" = true
  }
}

resource "aws_api_gateway_method" "swagger-method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.swagger-resource.id
  http_method   = "GET"
  authorization = "NONE"
  api_key_required = false
  request_parameters = {
    "method.request.path.proxy" = true
  }
}

resource "aws_api_gateway_integration" "api-integration" {
  rest_api_id          = aws_api_gateway_rest_api.api.id
  resource_id          = aws_api_gateway_resource.api-resource.id
  http_method          = aws_api_gateway_method.api-method.http_method
  integration_http_method = "POST"
  type                 = "AWS_PROXY"
  uri                  = aws_lambda_function.lambda_handler.invoke_arn
}

resource "aws_api_gateway_integration" "apidoc-integration" {
  rest_api_id          = aws_api_gateway_rest_api.api.id
  resource_id          = aws_api_gateway_resource.apidoc-resource.id
  http_method          = aws_api_gateway_method.apidoc-method.http_method
  integration_http_method = "POST"
  type                 = "AWS_PROXY"
  uri                  = aws_lambda_function.lambda_handler.invoke_arn
}

resource "aws_api_gateway_integration" "swagger-integration" {
  rest_api_id          = aws_api_gateway_rest_api.api.id
  resource_id          = aws_api_gateway_resource.swagger-resource.id
  http_method          = aws_api_gateway_method.swagger-method.http_method
  integration_http_method = "POST"
  type                 = "AWS_PROXY"
  uri                  = aws_lambda_function.lambda_handler.invoke_arn
}