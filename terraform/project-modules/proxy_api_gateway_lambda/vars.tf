variable name {
  type = string
}

variable aws_region {
  type = string
}

variable aws_env {
  type=string
}

variable aws_id {
  type=string
}

variable dynamodb_resources {
  type = list(string)
}

variable code_bucket_id {
  type = string
}

variable payloads_bucket_arn {
  type=string
}

variable folder {
  type = string
} 

variable tags {
  type = map
  default = {}
}

variable timeout {
  type = number
  default = 30
}

variable memory_size {
  type = number
  default = 512
}

variable endpoint_type {
  type = list
  default = ["REGIONAL"]
}

variable layers {
  type=list
  default = []
}

variable lbd_var_env {
  type=map
  default = {}
}
variable base_dns_domain {
  type = string
}

variable version_name {
  type = string
}

variable lambda_target_roles {
  type=list
  default=[]
}