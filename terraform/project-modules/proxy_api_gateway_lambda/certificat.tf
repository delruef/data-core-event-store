resource "aws_acm_certificate" "cert" {
  count = var.base_dns_domain != "" ? 1 : 0
  domain_name       = local.domain_name
  validation_method = "DNS"

  tags = var.tags

  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_acm_certificate_validation" "cert_validation" {
  count = var.base_dns_domain != "" ? 1 : 0
  certificate_arn         = aws_acm_certificate.cert[count.index].arn
  validation_record_fqdns = aws_route53_record.cert_validation.*.fqdn
}
