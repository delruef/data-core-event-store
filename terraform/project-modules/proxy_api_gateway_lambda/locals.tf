locals {
    domain_name = var.base_dns_domain != "" ? "${var.version_name}.event-store.${var.aws_env}.${var.base_dns_domain}" : null
}