
data "aws_route53_zone" "zone" {
  count = var.base_dns_domain != "" ? 1 : 0
  name         = "${var.aws_env}.${var.base_dns_domain}"
  private_zone = false
}

resource "aws_route53_record" "cert_validation" { #https://github.com/hashicorp/terraform-provider-aws/issues/14447
  count = var.base_dns_domain != "" ? 1 : 0
  name            = aws_acm_certificate.cert[count.index].domain_validation_options.*.resource_record_name[0]
  records         = [aws_acm_certificate.cert[count.index].domain_validation_options.*.resource_record_value[0]]
  type            = aws_acm_certificate.cert[count.index].domain_validation_options.*.resource_record_type[0]
  zone_id         = data.aws_route53_zone.zone[count.index].zone_id
  ttl             = 60
}


resource "aws_route53_record" "domain" {
  count = var.base_dns_domain != "" ? 1 : 0
  name    = aws_api_gateway_domain_name.domain_name[count.index].domain_name
  type    = "A"
  zone_id = data.aws_route53_zone.zone[count.index].zone_id

  alias {
    evaluate_target_health = true
    name                   = aws_api_gateway_domain_name.domain_name[count.index].regional_domain_name
    zone_id                = aws_api_gateway_domain_name.domain_name[count.index].regional_zone_id
  }
}