locals {
  s3_file_name = "${var.folder}/${var.name}.zip"
  function_name = "lbd-${var.name}"
}

resource "aws_iam_role" "handler-role" {
  name      = "rol-${var.name}"
  tags      =  merge(
    var.tags,
    {
      "Name" = "rol-${var.name}"
    })
  force_detach_policies = true
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_lambda_function" "lambda_handler" {
  function_name = local.function_name
  s3_bucket     = var.code_bucket_id
  s3_key        = local.s3_file_name
  handler       = "src.main.handler"
  runtime       = "python3.8"
  role          = aws_iam_role.handler-role.arn
  source_code_hash = base64sha256(local.s3_file_name)
  layers = var.layers
  memory_size = var.memory_size
  environment {
        variables = var.lbd_var_env
  }

  timeout       = var.timeout
  tags          = merge(var.tags,
    {
      "Name" = local.function_name
    })

  publish       = true

}


data "aws_iam_policy_document" "function-pol" {
  statement  {
    actions = ["logs:CreateLogStream","logs:PutLogEvents"]
    resources = ["arn:aws:logs:${var.aws_region}:${var.aws_id}:log-group:/aws/lambda/${aws_lambda_function.lambda_handler.function_name}:*"]
  }
  statement   {
    actions = ["logs:CreateLogGroup"]
    resources = ["arn:aws:logs:${var.aws_region}:${var.aws_id}:*"]
  }

  statement   {
    actions = [
      "dynamodb:PutItem",
      "dynamodb:GetItem",
      "dynamodb:UpdateItem",
      "dynamodb:DeleteItem",
      "dynamodb:Scan",
      "dynamodb:Query",
      "dynamodb:DescribeTable",
      "dynamodb:BatchGetItem",
      "dynamodb:BatchWriteItem"
    ]
    resources = var.dynamodb_resources

  }
  
  statement  {
    actions = [
      "s3:GetObject",
      "s3:PutObject",
    ]
    resources = [var.payloads_bucket_arn, "${var.payloads_bucket_arn}/*"]
  }

  statement {
    actions = [
      "sts:AssumeRole",
    ]
    resources = var.lambda_target_roles
  }
}

resource "aws_iam_policy" "function-pol" {
  name      = "pol-${var.name}-generic-api"
  policy    = data.aws_iam_policy_document.function-pol.json
}

resource "aws_iam_role_policy_attachment" "attach-pol" {
  role        = aws_iam_role.handler-role.name
  policy_arn  = aws_iam_policy.function-pol.arn
}

resource "aws_cloudwatch_log_group" "lambda_log_group" {
  name = "/aws/lambda/${aws_lambda_function.lambda_handler.function_name}"

  tags = merge(
    var.tags,
    {
      "Name" = "/aws/lambda/${aws_lambda_function.lambda_handler.function_name}"
    }
  )
}
