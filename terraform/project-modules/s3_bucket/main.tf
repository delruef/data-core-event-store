locals {
  name = "s3-${var.name}"
}

resource "aws_s3_bucket" "s3_bucket" {
  bucket = local.name
  force_destroy = true

  tags = merge(
    var.tags,
    {
      "Name" = local.name
    },
  )
  acl = "private"
}