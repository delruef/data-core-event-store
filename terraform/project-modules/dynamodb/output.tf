output "id" {
    value = aws_dynamodb_table.dynamodb_table.id
}

output "arn" {
    value = aws_dynamodb_table.dynamodb_table.arn
}