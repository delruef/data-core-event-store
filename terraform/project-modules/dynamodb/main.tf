
locals {
    table_name = "ddb-${var.name}"
}

resource "aws_dynamodb_table" "dynamodb_table" {

  name           = local.table_name
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "uuid"
  range_key      = "version"

  attribute {
    name = "uuid"
    type = "S"
  }
  
  attribute {
    name = "version"
    type = "S"
  }
  
  attribute {
    name = "correlation_id"
    type = "S"
  }
  
  attribute {
    name = "name_data_id"
    type = "S"
  }
  
  attribute {
    name = "name"
    type = "S"
  }

  attribute {
    name = "updated_at"
    type = "S"
  }

  global_secondary_index {
    name               = "version_index"
    hash_key           = "version"
    range_key          = "updated_at"
    projection_type    = "ALL"
  }

  global_secondary_index {
    name               = "correlation_id_index"
    hash_key           = "correlation_id"
    range_key          = "version"
    projection_type    = "ALL"
  }
  
  global_secondary_index {
    name               = "name_data_id_index"
    hash_key           = "name_data_id"
    range_key          = "updated_at"
    projection_type    = "ALL"
  }
  
  global_secondary_index {
    name               = "name_index"
    hash_key           = "name"
    range_key          = "updated_at"
    projection_type    = "ALL"
  }

  global_secondary_index {
    name               = "label_index"
    hash_key           = "name"
    range_key          = "updated_at"
    projection_type    = "ALL"
  }

  tags = merge(
    var.tags,
    {
      "Name" = local.table_name
    })
}

