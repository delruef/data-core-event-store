# Terega event store api

## Utilisation en locale

### Installation

- Télécharger docker
- Cloner le fichier .env.example en le nommant .env.development
- Modifier les variables d'env si nécessaire (DYNAMODB_HOST, ...)

  le port à utiliser doit être le même que celui indiqué dans le fichier docker-compose.yml (8000 pour la db par défaut)

- [Installer un environnement virtuel](https://docs.python.org/fr/3/library/venv.html#creating-virtual-environments)
- activer l'environnement virtuel
- une fois le venv activé
  
  `pip install poetry`

  `poetry install`

### Démarrage

- Lancer le container de dynamodb-local

  `docker-compose up -d`

- Lancer l'appplication

  `uvicorn src.main:app --reload --port 5000` (ou un autre port)

### Utilisation

Utiliser Postman (ou autre) pour effectuer les req sur l'api

Pour intéragir avec les données dans la db, utiliser un outil comme [celui-ci](https://github.com/aaronshaf/dynamodb-admin) ou équivalent

### Test

- Cloner le fichier .env.example en le nommant .env.testing
- Modifier les variables d'env si nécessaire (DYNAMODB_HOST, ...)

  le port à utiliser doit être le même que celui indiqué dans le fichier docker-compose.yml (8002 pour la db de test par défaut)
