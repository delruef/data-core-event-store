layer_tmp_zip_file=layer.zip
lambda_tmp_zip_file=lambda.zip

{
    apt-get install -y zip
    cd $PWD/event_store_api
    pip install poetry
    python3 -m venv deploy-venv
    source deploy-venv/bin/activate
    poetry install --no-dev --no-root
    deactivate
    mkdir python
    mv deploy-venv/lib/python3.8/site-packages/* python
    find python src -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete
    zip -ru $layer_tmp_zip_file python
    zip -ru $lambda_tmp_zip_file src
    aws s3 cp $layer_tmp_zip_file s3://$bucket/$layer_file_name
    aws s3 cp $lambda_tmp_zip_file s3://$bucket/$lambda_file_name

    rm -rf python
    rm -rf deploy-venv
    rm $layer_tmp_zip_file
    rm $lambda_tmp_zip_file
    cd $PWD
} || {
    rm -rf $PWD/event_store_api/python || true
    rm -rf $PWD/event_store_api/deploy-venv || true
    rm $PWD/event_store_api/$layer_tmp_zip_file || true
    rm $PWD/event_store_api/$lambda_tmp_zip_file || true
    exit 1
}
