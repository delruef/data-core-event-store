#!/bin/bash
set -e

source event_store_api/.bin/initialize.sh $1 $2

event_store_api/.bin/do_upload_file.sh

aws lambda publish-layer-version \
    --layer-name $layer_function_name \
    --content S3Bucket=$bucket,S3Key=$layer_file_name

last_layer_arn=$(aws lambda list-layer-versions \
    --layer-name $layer_function_name \
    --region $AWS_DEFAULT_REGION \
    --query 'LayerVersions[0].LayerVersionArn' | tr -d '"')

aws lambda update-function-configuration \
    --function-name $lambda_function_name \
    --layers $last_layer_arn

sleep 5

aws lambda update-function-code \
    --function-name $lambda_function_name \
    --s3-bucket $bucket \
    --s3-key $lambda_file_name \
    --publish
