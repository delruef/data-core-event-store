#!/bin/bash
set -e

apt-get update && apt-get install -y jq
pip install awscli

lambda_folder=lambdas
layer_folder=layers
runtime=app

env=$1
dashed_version=$2
suffix=$TF_VAR_code_soc-$runtime-$TF_VAR_code_app-$env-$dashed_version

# mkdir -p ~/.aws/
# rm -f ~/.aws/credentials

eval $(aws sts assume-role --role-arn arn:aws:iam::$TF_VAR_aws_id:role/rol-$TF_VAR_code_soc-$runtime-$TF_VAR_code_app-$env-deploy --external-id $EXTERNAL_ID --role-session-name test | jq -r '.Credentials | "export AWS_ACCESS_KEY_ID=\(.AccessKeyId)\nexport AWS_SECRET_ACCESS_KEY=\(.SecretAccessKey)\nexport AWS_SESSION_TOKEN=\(.SessionToken)\n"')

# echo [profile deployer] >> ~/.aws/credentials
# echo role_arn = arn:aws:iam::$TF_VAR_aws_id:role/rol-s3-lyr-lbd-$TF_VAR_code_soc-$runtime-$TF_VAR_code_app-$env-deployer >> ~/.aws/credentials

# unset AWS_DEFAULT_PROFILE
# export AWS_CONFIG_FILE=~/.aws/credentials
# export AWS_PROFILE=deployer
export AWS_DEFAULT_REGION=$TF_VAR_aws_region
export layer_function_name=lyr-$suffix
export lambda_function_name=lbd-$suffix
export bucket=s3-code-$suffix
export layer_file_name=$layer_folder/$suffix.zip
export lambda_file_name=$lambda_folder/$suffix.zip
