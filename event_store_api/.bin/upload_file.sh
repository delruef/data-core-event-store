#!/bin/bash
set -e

source event_store_api/.bin/initialize.sh $1 $2

event_store_api/.bin/do_upload_file.sh