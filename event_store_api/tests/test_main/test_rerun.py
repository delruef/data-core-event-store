from uuid import uuid4

from pydash import set_

from src.config import (
    StatusEnum,
    KindEnum,
)
from src import dao
from ..utils.boto3_client_mock import Boto3ClientMock
from . import config, client, default_resource_config, default_payload


def test_rerun_with_second_level_children(mocker):
    parent_id = str(uuid4())
    correlation_id = str(uuid4())

    parent_common_payload = {
        "name": "some_parent_name",
        "label": "some_parent_label",
        "data_id": str(uuid4()),
        "correlation_id": correlation_id,
        "parent_id": correlation_id,
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
    }

    # create root parent
    dao.create(
        uuid=correlation_id,
        name="some_root_parent_name",
        label="some_root_parent_label",
        data_id=str(uuid4()),
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    # create parent
    parent = dao.create(
        **parent_common_payload,
        uuid=parent_id,
        status=StatusEnum.FINISHED,
        payload_file_name=str(uuid4()),
    )
    # create level 1 child 1
    level1_child1 = dao.create(
        name="some_level1_child1_name",
        label="some_level1_child1_label",
        data_id=str(uuid4()),
        correlation_id=correlation_id,
        parent_id=parent_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
        status=StatusEnum.FINISHED,
    )
    # create level 1 child 2
    level1_child2 = dao.create(
        name="some_level1_child2_name",
        label="some_level1_child2_label",
        data_id=str(uuid4()),
        correlation_id=correlation_id,
        parent_id=parent_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
        status=StatusEnum.FINISHED,
    )

    level2_child1 = dao.create(
        name="some_level2_child1_name",
        label="some_level2_child1_label",
        data_id=str(uuid4()),
        correlation_id=correlation_id,
        parent_id=level1_child1.get("uuid"),
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
        status=StatusEnum.FINISHED,
    )
    # create level 1 child 2
    level2_child2 = dao.create(
        name="some_level2_child2_name",
        label="some_level2_child2_label",
        data_id=str(uuid4()),
        correlation_id=correlation_id,
        parent_id=level1_child1.get("uuid"),
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
        status=StatusEnum.ERROR,
    )

    payload_with_event_id = default_payload.copy()
    set_(payload_with_event_id, "eventId", parent_id)

    mocker.patch(
        "boto3.client",
        return_value=Boto3ClientMock(
            config, parent.get("resource_config"), payload_with_event_id, parent
        ),
    )
    client.post(
        f"/events/{parent_id}/rerun",
    )

    client.put(
        f"/events/{parent_id}",
        json={**parent_common_payload, "payload": default_payload},
    )
    parent_all_versions = dao.query(parent.get("uuid"), range_key_condition=None)
    level1_child1_all_versions = dao.query(
        level1_child1.get("uuid"), range_key_condition=None
    )
    level1_child2_all_versions = dao.query(
        level1_child2.get("uuid"), range_key_condition=None
    )
    level2_child1_all_versions = dao.query(
        level2_child1.get("uuid"), range_key_condition=None
    )
    level2_child2_all_versions = dao.query(
        level2_child2.get("uuid"), range_key_condition=None
    )

    assert len(parent_all_versions["items"]) == 3
    assert parent_all_versions["items"][0].get("version") == "v_0"
    assert parent_all_versions["items"][0].get("status") == StatusEnum.PENDING
    assert parent_all_versions["items"][1].get("version") == "v_1"
    assert parent_all_versions["items"][1].get("status") == StatusEnum.FINISHED
    assert parent_all_versions["items"][2].get("version") == "v_2"
    assert parent_all_versions["items"][2].get("status") == StatusEnum.ACCEPTED

    assert len(level1_child1_all_versions["items"]) == 1
    assert level1_child1_all_versions["items"][0].get("version") == "v_1"

    assert len(level1_child2_all_versions["items"]) == 1
    assert level1_child2_all_versions["items"][0].get("version") == "v_1"

    assert len(level2_child1_all_versions["items"]) == 1
    assert level2_child1_all_versions["items"][0].get("version") == "v_1"

    assert len(level2_child2_all_versions["items"]) == 1
    assert level2_child2_all_versions["items"][0].get("version") == "v_1"
