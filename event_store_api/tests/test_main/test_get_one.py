from uuid import uuid4

from src.config import (
    KindEnum,
)
from src import dao
from . import client, default_resource_config


def test_get_one():
    correlation_id = str(uuid4())
    item = dao.create(
        uuid=correlation_id,
        name="item_name",
        label="item_label",
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.get(f"/events/{correlation_id}")

    assert res.status_code == 200
    assert item.items() <= res.json().items()
