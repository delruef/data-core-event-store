import json
from uuid import uuid4

from fastapi.testclient import TestClient

from src.main import app
from src.config import (
    MethodsEnum,
    get_config,
)

client = TestClient(app)
config = get_config()
config.LAMBDA_ROLES = [
    "arn:aws:iam::111111111111:role/rol-crossaccount-env-api-interfaces",
    "arn:aws:iam::111111111111:role/rol-crossaccount-env-pegaz",
    "arn:aws:iam::111111111111:role/rol-crossaccount-env-dataex",
    "arn:aws:iam::222222222222:role/rol-crossaccount-las-env-api-interfaces",
    "arn:aws:iam::222222222222:role/rol-crossaccount-las-env-dataex",
]

default_resource_config = {
    "function_name": """
        arn:aws:lambda:eu-west-1:111111111111:function:lbd-core-dataex-loc-to-smartsheets-env""",
}

default_payload = {
    "resource": "some_resource",
    "httpMethod": MethodsEnum.POST,
    "pathParameters": {"uuid": str(uuid4())},
    "queryStringParameters": {"q_param": "q_param_value"},
    "body": json.dumps({"b_param": "b_param_value"}),
}
