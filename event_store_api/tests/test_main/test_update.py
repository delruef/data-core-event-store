from uuid import uuid4

from pydash import set_

from src.config import (
    StatusEnum,
    KindEnum,
)
from src import dao
from ..utils.boto3_client_mock import Boto3ClientMock
from . import config, client, default_resource_config, default_payload


def test_update_accepted_event_while_similar_pending_event_exists_and_allow_multiple():
    data_id = str(uuid4())
    uuid = str(uuid4())
    queued_uuid = str(uuid4())
    dao.create(
        uuid=queued_uuid,
        data_id=data_id,
        name="some_name",
        label="some_label",
        correlation_id=queued_uuid,
        status=StatusEnum.PENDING,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    payload = {
        "data_id": data_id,
        "name": "some_name",
        "label": "some_label",
        "correlation_id": uuid,
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
        "allow_multiple": True,
    }

    event_store = dao.create(
        **payload,
        status=StatusEnum.ACCEPTED,
        uuid=uuid,
        payload_file_name=str(uuid4()),
    )

    res = client.put(
        f"/events/{uuid}",
        json={**payload, "payload": default_payload},
    )

    expected_res = {**event_store, **payload, "status": StatusEnum.PENDING}
    events = dao.query(uuid, range_key_condition=None)
    res_json = res.json()

    del res_json["payload_file_name"]
    del expected_res["payload_file_name"]
    del expected_res["created_at"]
    del expected_res["updated_at"]

    assert res.status_code == 200
    assert expected_res.items() <= res_json.items()
    assert len(events["items"]) == 2
    assert events["items"][1]["version"] == "v_1"
    assert events["items"][1]["status"] == StatusEnum.ACCEPTED


def test_update_accepted_event_while_similar_pending_to_updating_event_exists_and_allow_multiple():
    data_id = str(uuid4())
    uuid = str(uuid4())
    queued_uuid = str(uuid4())
    dao.create(
        uuid=queued_uuid,
        data_id=data_id,
        name="some_name",
        label="some_label",
        correlation_id=queued_uuid,
        status=StatusEnum.PENDING,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    payload = {
        "data_id": data_id,
        "name": "some_name",
        "label": "some_label",
        "correlation_id": uuid,
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
        "allow_multiple": True,
    }

    event_store = dao.create(
        **payload,
        status=StatusEnum.ACCEPTED,
        uuid=uuid,
        queue_ids=[queued_uuid],
        payload_file_name=str(uuid4()),
    )

    res = client.put(
        f"/events/{uuid}",
        json={**payload, "payload": default_payload},
    )

    expected_res = {**event_store, **payload, "status": StatusEnum.PENDING}
    events = dao.query(uuid, range_key_condition=None)
    res_json = res.json()

    del res_json["payload_file_name"]
    del expected_res["payload_file_name"]
    del expected_res["created_at"]
    del expected_res["updated_at"]

    assert res.status_code == 200
    assert expected_res.items() <= res_json.items()
    assert len(events["items"]) == 2
    assert events["items"][1]["version"] == "v_1"
    assert events["items"][1]["status"] == StatusEnum.ACCEPTED


def test_update_accepted_event_while_similar_pending_event_exists_and_not_allow_multiple():
    data_id = str(uuid4())
    uuid = str(uuid4())
    queued_uuid = str(uuid4())
    dao.create(
        uuid=queued_uuid,
        data_id=data_id,
        name="some_name",
        label="some_label",
        correlation_id=queued_uuid,
        status=StatusEnum.PENDING,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    payload = {
        "data_id": data_id,
        "name": "some_name",
        "label": "some_label",
        "correlation_id": uuid,
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
        "allow_multiple": False,
    }

    event_store = dao.create(
        **payload,
        status=StatusEnum.ACCEPTED,
        uuid=uuid,
        payload_file_name=str(uuid4()),
    )

    res = client.put(
        f"/events/{uuid}",
        json={**payload, "payload": default_payload},
    )

    expected_res = {**event_store, **payload, "status": StatusEnum.QUEUED}
    events = dao.query(uuid, range_key_condition=None)
    res_json = res.json()

    del res_json["payload_file_name"]
    del expected_res["payload_file_name"]
    del expected_res["created_at"]
    del expected_res["updated_at"]

    assert res.status_code == 200
    assert expected_res.items() <= res_json.items()
    assert len(events["items"]) == 2
    assert events["items"][1]["version"] == "v_1"
    assert events["items"][1]["status"] == StatusEnum.ACCEPTED


def test_update_accepted_event_while_similar_pending_to_updating_event_exists_and_not_al_multiple():
    data_id = str(uuid4())
    uuid = str(uuid4())
    queued_uuid = str(uuid4())
    dao.create(
        uuid=queued_uuid,
        data_id=data_id,
        name="some_name",
        label="some_label",
        correlation_id=queued_uuid,
        status=StatusEnum.PENDING,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    payload = {
        "data_id": data_id,
        "name": "some_name",
        "label": "some_label",
        "correlation_id": uuid,
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
        "allow_multiple": False,
    }

    event_store = dao.create(
        **payload,
        status=StatusEnum.ACCEPTED,
        uuid=uuid,
        payload_file_name=str(uuid4()),
        queue_ids=[queued_uuid],
    )

    res = client.put(
        f"/events/{uuid}",
        json={**payload, "payload": default_payload},
    )

    expected_res = {**event_store, **payload, "status": StatusEnum.PENDING}
    events = dao.query(uuid, range_key_condition=None)
    res_json = res.json()

    del res_json["payload_file_name"]
    del expected_res["payload_file_name"]
    del expected_res["created_at"]
    del expected_res["updated_at"]

    assert res.status_code == 200
    assert expected_res.items() <= res_json.items()
    assert len(events["items"]) == 2
    assert events["items"][1]["version"] == "v_1"
    assert events["items"][1]["status"] == StatusEnum.ACCEPTED


def test_update_pending_event_having_queued_ids(mocker):
    queued_uuid = str(uuid4())
    queued_uuid_2 = str(uuid4())
    uuid = str(uuid4())

    payload = {
        "status": StatusEnum.FINISHED,
        "message": "some message",
    }

    queued_event = {
        "uuid": queued_uuid,
        "data_id": "some_data_id",
        "name": "some_name",
        "label": "some_label",
        "correlation_id": queued_uuid,
        "status": StatusEnum.PENDING,
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
        "payload_file_name": str(uuid4()),
    }

    queued_event_2 = {
        "uuid": queued_uuid_2,
        "data_id": "some_data_id",
        "name": "some_name",
        "label": "some_label",
        "correlation_id": queued_uuid_2,
        "status": StatusEnum.PENDING,
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
        "payload_file_name": str(uuid4()),
    }
    dao.create(**queued_event)
    dao.create(**queued_event_2)

    payload_with_event_id = default_payload.copy()
    set_(payload_with_event_id, "eventId", queued_uuid)
    mocker.patch(
        "boto3.client",
        return_value=Boto3ClientMock(
            config,
            queued_event.get("resource_config"),
            payload_with_event_id,
            queued_event,
        ),
    )

    event_store = dao.create(
        uuid=uuid,
        data_id="some_data_id",
        name="some_name",
        label="some_label",
        correlation_id=uuid,
        status=StatusEnum.PENDING,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
        queue_ids=[queued_uuid, queued_uuid_2],
    )

    res = client.patch(
        f"/events/{uuid}",
        json=payload,
    )

    expected_res = {**event_store, **payload, "queue_ids": []}
    del expected_res["updated_at"]

    updated_queued_event = dao.query(queued_uuid)
    events = dao.query(uuid, range_key_condition=None)

    assert res.status_code == 200
    assert expected_res.items() <= res.json().items()
    assert updated_queued_event["items"][0].get("status") == StatusEnum.ACCEPTED
    assert updated_queued_event["items"][0].get("queue_ids") == [queued_uuid_2]

    assert events["items"][1]["version"] == "v_1"
    assert events["items"][1]["status"] == event_store.get("status")
