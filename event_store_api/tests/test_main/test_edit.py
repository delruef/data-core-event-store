from uuid import uuid4

from src.config import StatusEnum, KindEnum, ErrorsEnum
from src import dao
from . import (
    client,
    default_resource_config,
)


def test_update_not_pending_event_status():
    payload = {
        "status": StatusEnum.FINISHED,
        "message": "some message",
    }
    uuid = str(uuid4())
    dao.create(
        uuid=uuid,
        data_id=str(uuid4()),
        name="some_name",
        label="some_label",
        status=StatusEnum.ERROR,
        correlation_id=uuid,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.patch(
        f"/events/{uuid}",
        json=payload,
    )

    assert res.status_code == 422
    assert res.json().get("detail") == ErrorsEnum.EVENT_IS_NEITHER_ACCEPTED_NOR_PENDING


def test_update_pending_event_with_success_status():
    uuid = str(uuid4())
    payload = {
        "status": StatusEnum.FINISHED,
        "message": "some message",
    }
    event_store = dao.create(
        uuid=uuid,
        data_id=str(uuid4()),
        name="some_name",
        label="some_label",
        correlation_id=uuid,
        status=StatusEnum.PENDING,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.patch(
        f"/events/{uuid}",
        json=payload,
    )

    expected_res = {**event_store, **payload}
    del expected_res["updated_at"]

    events = dao.query(uuid, range_key_condition=None)

    assert res.status_code == 200
    assert expected_res.items() <= res.json().items()
    assert len(events["items"]) == 2
    assert events["items"][1]["version"] == "v_1"
    assert events["items"][1]["status"] == event_store.get("status")


def test_update_pending_event_with_error_status():
    uuid = str(uuid4())
    payload = {
        "status": StatusEnum.ERROR,
        "message": "some message",
    }
    event_store = dao.create(
        uuid=uuid,
        data_id=str(uuid4()),
        name="some_name",
        label="some_label",
        correlation_id=uuid,
        status=StatusEnum.PENDING,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.patch(
        f"/events/{uuid}",
        json=payload,
    )

    expected_res = {**event_store, **payload}
    del expected_res["updated_at"]

    events = dao.query(uuid, range_key_condition=None)

    assert res.status_code == 200
    assert expected_res.items() < res.json().items()
    assert events["items"][1]["version"] == "v_1"
    assert events["items"][1]["status"] == event_store.get("status")


def test_update_stopped_event_queue_ids():
    payload = {
        "queue_ids": [str(uuid4())],
    }
    uuid = str(uuid4())
    dao.create(
        uuid=uuid,
        data_id=str(uuid4()),
        name="some_name",
        label="some_label",
        status=StatusEnum.ERROR,
        correlation_id=uuid,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.patch(
        f"/events/{uuid}",
        json=payload,
    )

    assert res.status_code == 422
    assert res.json().get("detail") == ErrorsEnum.CANNOT_EDIT_STOPPED_EVENT_QUEUE_IDS


def test_update_event_queue_ids_with_list():
    uuid = str(uuid4())
    queue_ids = [str(uuid4()), str(uuid4())]
    payload = {
        "queue_ids": queue_ids,
    }
    event_store = dao.create(
        uuid=uuid,
        data_id=str(uuid4()),
        name="some_name",
        label="some_label",
        correlation_id=uuid,
        status=StatusEnum.PENDING,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
        queue_ids=[str(uuid4())],
    )

    res = client.patch(
        f"/events/{uuid}",
        json=payload,
    )

    expected_res = {**event_store, **payload}
    del expected_res["updated_at"]

    events = dao.query(uuid, range_key_condition=None)

    assert res.status_code == 200
    assert expected_res.items() <= res.json().items()
    assert len(events["items"]) == 2
    assert events["items"][1]["version"] == "v_1"
    assert events["items"][1]["queue_ids"] == event_store.get("queue_ids")


def test_update_event_queue_ids_with_add():
    uuid = str(uuid4())
    queue_ids = [str(uuid4()), str(uuid4()), str(uuid4())]
    add = [str(uuid4()), str(uuid4())]
    payload = {
        "queue_ids": {
            "add": add,
        },
    }
    event_store = dao.create(
        uuid=uuid,
        data_id=str(uuid4()),
        name="some_name",
        label="some_label",
        correlation_id=uuid,
        status=StatusEnum.PENDING,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
        queue_ids=queue_ids,
    )

    res = client.patch(
        f"/events/{uuid}",
        json=payload,
    )

    expected_res = {**event_store, "queue_ids": [*queue_ids, *add]}
    del expected_res["updated_at"]

    events = dao.query(uuid, range_key_condition=None)

    assert res.status_code == 200
    assert expected_res.items() <= res.json().items()
    assert len(events["items"]) == 2
    assert events["items"][1]["version"] == "v_1"
    assert events["items"][1]["queue_ids"] == event_store.get("queue_ids")
