import json
from uuid import uuid4

from src.config import KindEnum, PaginationEnum, StatusEnum
from src import dao
from . import client, default_resource_config


def test_empty_get_many():
    res = client.get("/events")
    assert res.status_code == 200
    assert (
        res.json().items()
        == {
            "limit": PaginationEnum.DEFAULT_LIMIT,
            "last_evaluated_key": None,
            "items": [],
        }.items()
    )


def test_get_many():
    correlation_id = str(uuid4())
    item1 = dao.create(
        uuid=correlation_id,
        name="item1_name",
        label="item1_label",
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    item2 = dao.create(
        name="item2_name",
        label="item2_label",
        correlation_id=correlation_id,
        parent_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.get("/events")
    res_json = res.json()

    assert res.status_code == 200
    assert res_json["limit"] == PaginationEnum.DEFAULT_LIMIT
    assert res_json["last_evaluated_key"] is None
    assert item2.items() <= res_json["items"][0].items()
    assert item1.items() <= res_json["items"][1].items()


def test_get_many_with_filters():
    correlation_id = str(uuid4())
    item1 = dao.create(
        uuid=correlation_id,
        name="item1_name",
        label="item1_label",
        data_id="data1_id",
        status=StatusEnum.QUEUED,
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    dao.create(
        name="item2_name",
        label="item2_label",
        correlation_id=correlation_id,
        parent_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.get(
        "/events",
        params={
            "data_id": item1.get("data_id"),
            "status": StatusEnum.QUEUED,
        },
    )
    res_json = res.json()

    assert res.status_code == 200
    assert len(res_json.get("items")) == 1
    assert res_json["limit"] == PaginationEnum.DEFAULT_LIMIT
    assert res_json["last_evaluated_key"] is None
    assert item1.items() <= res_json["items"][0].items()


def test_paginated_get_many():
    correlation_id = str(uuid4())
    dao.create(
        uuid=correlation_id,
        name="item1_name",
        label="item1_label",
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    item2 = dao.create(
        name="item2_name",
        label="item2_label",
        correlation_id=correlation_id,
        parent_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    item3 = dao.create(
        name="item3_name",
        label="item3_label",
        correlation_id=correlation_id,
        parent_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.get(
        "/events",
        params={
            "limit": 1,
            "last_evaluated_key": json.dumps(
                {
                    "uuid": {"S": item3.get("uuid")},
                    "updated_at": {"S": item3.get("updated_at")},
                    "version": {"S": "v_0"},
                }
            ),
        },
    )
    expected_last_evaluated_key = {
        "uuid": {"S": item2.get("uuid")},
        "updated_at": {"S": item2.get("updated_at")},
        "version": {"S": "v_0"},
    }
    res_json = res.json()
    assert res.status_code == 200
    assert res_json["limit"] == 1
    assert expected_last_evaluated_key.items() == res_json["last_evaluated_key"].items()
    assert len(res_json["items"]) == 1
    assert item2.items() <= res_json["items"][0].items()


def test_get_many_by_name_with_filters():
    name = "item_name"
    label = "item_label"
    data_id = "data_id"
    correlation_id = str(uuid4())
    item1 = dao.create(
        name=name,
        label=label,
        data_id=data_id,
        status=StatusEnum.QUEUED,
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    dao.create(
        name=name,
        label=label,
        data_id=data_id,
        correlation_id=correlation_id,
        parent_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.get(
        "/events",
        params={
            "name": name,
            "data_id": data_id,
            "status": StatusEnum.QUEUED,
            "label": label,
        },
    )
    res_json = res.json()

    assert res.status_code == 200
    assert len(res_json.get("items")) == 1
    assert res_json["limit"] == PaginationEnum.DEFAULT_LIMIT
    assert res_json["last_evaluated_key"] is None
    assert item1.items() <= res_json["items"][0].items()


def test_paginated_get_many_by_name():
    correlation_id = str(uuid4())
    name = "item_name"
    label = "item_label"

    dao.create(
        uuid=correlation_id,
        name=name,
        label=label,
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    dao.create(
        uuid=correlation_id,
        version="v_1",
        name=name,
        label=label,
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    item1 = dao.create(
        name=name,
        label=label,
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    item2 = dao.create(
        name=name,
        label=label,
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.get(
        "/events",
        params={
            "name": name,
            "limit": 1,
            "last_evaluated_key": json.dumps(
                {
                    "uuid": {"S": item2.get("uuid")},
                    "updated_at": {"S": item2.get("updated_at")},
                    "version": {"S": "v_0"},
                    "name": {"S": name},
                }
            ),
        },
    )

    expected_last_evaluated_key = {
        "uuid": {"S": item1.get("uuid")},
        "updated_at": {"S": item1.get("updated_at")},
        "version": {"S": "v_0"},
        "name": {"S": name},
    }
    res_json = res.json()

    assert res.status_code == 200
    assert res_json["limit"] == 1
    assert expected_last_evaluated_key.items() == res_json["last_evaluated_key"].items()
    assert len(res_json["items"]) == 1
    assert item1.items() <= res_json["items"][0].items()


def test_paginated_get_many_by_name_and_data_id():
    correlation_id = str(uuid4())
    name = "item_name"
    label = "item_label"
    data_id = str(uuid4())

    dao.create(
        uuid=correlation_id,
        name=name,
        label=label,
        data_id=data_id,
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    dao.create(
        uuid=correlation_id,
        version="v_1",
        name=name,
        label=label,
        data_id=data_id,
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    item1 = dao.create(
        name=name,
        label=label,
        data_id=data_id,
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    item2 = dao.create(
        name=name,
        label=label,
        data_id=data_id,
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.get(
        "/events",
        params={
            "name": name,
            "data_id": data_id,
            "limit": 1,
            "last_evaluated_key": json.dumps(
                {
                    "uuid": {"S": item2.get("uuid")},
                    "updated_at": {"S": item2.get("updated_at")},
                    "version": {"S": "v_0"},
                    "name_data_id": {"S": f"{name}#{data_id}"},
                }
            ),
        },
    )

    expected_last_evaluated_key = {
        "uuid": {"S": item1.get("uuid")},
        "updated_at": {"S": item1.get("updated_at")},
        "version": {"S": "v_0"},
        "name_data_id": {"S": f"{name}#{data_id}"},
    }
    res_json = res.json()

    assert res.status_code == 200
    assert res_json["limit"] == 1
    assert expected_last_evaluated_key.items() == res_json["last_evaluated_key"].items()
    assert len(res_json["items"]) == 1
    assert item1.items() <= res_json["items"][0].items()


def test_get_many_by_label_with_filters():
    name = "item_name"
    label = "item_label"
    data_id = "data_id"
    correlation_id = str(uuid4())
    item1 = dao.create(
        name=name,
        label=label,
        data_id=data_id,
        status=StatusEnum.QUEUED,
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    dao.create(
        name=name,
        label=label,
        data_id=data_id,
        correlation_id=correlation_id,
        parent_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.get(
        "/events",
        params={
            "label": label,
            "data_id": data_id,
            "status": StatusEnum.QUEUED,
        },
    )
    res_json = res.json()

    assert res.status_code == 200
    assert len(res_json.get("items")) == 1
    assert res_json["limit"] == PaginationEnum.DEFAULT_LIMIT
    assert res_json["last_evaluated_key"] is None
    assert item1.items() <= res_json["items"][0].items()


def test_paginated_get_many_by_label():
    correlation_id = str(uuid4())
    name = "item_name"
    label = "item_label"

    dao.create(
        uuid=correlation_id,
        name=name,
        label=label,
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    dao.create(
        uuid=correlation_id,
        version="v_1",
        name=name,
        label=label,
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    item1 = dao.create(
        name=name,
        label=label,
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    item2 = dao.create(
        name=name,
        label=label,
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.get(
        "/events",
        params={
            "label": label,
            "limit": 1,
            "last_evaluated_key": json.dumps(
                {
                    "uuid": {"S": item2.get("uuid")},
                    "updated_at": {"S": item2.get("updated_at")},
                    "version": {"S": "v_0"},
                    "label": {"S": label},
                }
            ),
        },
    )

    expected_last_evaluated_key = {
        "uuid": {"S": item1.get("uuid")},
        "updated_at": {"S": item1.get("updated_at")},
        "version": {"S": "v_0"},
        "label": {"S": label},
    }
    res_json = res.json()

    assert res.status_code == 200
    assert res_json["limit"] == 1
    assert expected_last_evaluated_key.items() == res_json["last_evaluated_key"].items()
    assert len(res_json["items"]) == 1
    assert item1.items() <= res_json["items"][0].items()
