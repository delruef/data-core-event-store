from uuid import uuid4
from datetime import datetime, timezone, timedelta

from src.config import (
    StatusEnum,
    KindEnum,
)
from src import dao
from . import client, default_resource_config, default_payload


def test_create():
    payload = {
        "uuid": str(uuid4()),
        "data_id": str(uuid4()),
        "name": "some_name",
        "label": "some_label",
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
        "payload": default_payload,
    }
    res = client.post(
        "/events",
        json=payload,
    )
    expected_res = {
        **payload,
        "status": StatusEnum.PENDING,
        "version": "v_0",
    }

    res_json = res.json()
    del res_json["payload_file_name"]
    del expected_res["payload"]

    assert res.status_code == 201
    assert expected_res.items() < res_json.items()
    assert isinstance(res_json["correlation_id"], str)
    assert res_json["correlation_id"] == res_json["uuid"]

    res = client.get("/events")

    assert res.status_code == 200
    assert expected_res.items() < res.json()["items"][0].items()


def test_create_with_no_data_id():
    payload = {
        "name": "some_name",
        "label": "some_label",
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
        "payload": default_payload,
    }
    res = client.post(
        "/events",
        json=payload,
    )
    expected_res = {
        **payload,
        "status": StatusEnum.PENDING,
        "version": "v_0",
    }
    res_json = res.json()
    del res_json["payload_file_name"]
    del expected_res["payload"]

    assert res.status_code == 201
    assert expected_res.items() < res_json.items()


def test_create_with_child():
    correlation_id = str(uuid4())
    payload = {
        "parent_id": correlation_id,
        "data_id": str(uuid4()),
        "name": "some_name",
        "label": "some_label",
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
        "payload": default_payload,
    }
    # create parent
    dao.create(
        uuid=correlation_id,
        name="some_parent_name",
        label="some_parent_label",
        data_id=str(uuid4()),
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    res = client.post(
        "/events",
        json=payload,
    )

    expected_res = {
        **payload,
        "correlation_id": correlation_id,
        "parent_id": correlation_id,
        "status": StatusEnum.PENDING,
        "version": "v_0",
    }

    res_json = res.json()
    del res_json["payload_file_name"]
    del expected_res["payload"]

    assert res.status_code == 201
    assert expected_res.items() < res_json.items()


def test_create_with_second_level_child():
    parent_id = str(uuid4())
    correlation_id = str(uuid4())

    payload = {
        "parent_id": parent_id,
        "data_id": str(uuid4()),
        "name": "some_name",
        "label": "some_label",
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
        "payload": default_payload,
    }
    # create root parent
    dao.create(
        uuid=correlation_id,
        name="some_root_parent_name",
        label="some_root_parent_label",
        data_id=str(uuid4()),
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    # create parent
    dao.create(
        uuid=parent_id,
        name="some_parent_name",
        label="some_parent_label",
        data_id=str(uuid4()),
        correlation_id=correlation_id,
        parent_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )
    res = client.post(
        "/events",
        json=payload,
    )

    expected_res = {
        **payload,
        "correlation_id": correlation_id,
        "parent_id": parent_id,
        "status": StatusEnum.PENDING,
        "version": "v_0",
    }

    res_json = res.json()
    del res_json["payload_file_name"]
    del expected_res["payload"]

    assert res.status_code == 201
    assert expected_res.items() < res_json.items()


def test_create_while_event_pending_exists():
    payload = {
        "data_id": str(uuid4()),
        "name": "some_name",
        "label": "some_label",
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
        "payload": default_payload,
        "allow_multiple": False,
    }
    uuid_newest = str(uuid4())
    uuid_oldest = str(uuid4())
    # create an already pending event
    dao.create(
        uuid=uuid_newest,
        data_id=payload.get("data_id"),
        name=payload.get("name"),
        label="some_other_label",
        correlation_id=uuid_newest,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    # create an already pending event but older than the first one
    dao.create(
        uuid=uuid_oldest,
        data_id=payload.get("data_id"),
        name=payload.get("name"),
        label="some_other_label",
        correlation_id=uuid_oldest,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
        updated_at=datetime.now(timezone.utc) - timedelta(minutes=1),
    )

    res = client.post(
        "/events",
        json=payload,
    )

    expected_res = {
        **payload,
        "status": StatusEnum.QUEUED,
        "version": "v_0",
    }

    res_json = res.json()
    del res_json["payload_file_name"]
    del expected_res["payload"]
    newests = dao.query(uuid_newest)

    assert res.status_code == 201
    assert expected_res.items() < res_json.items()
    assert newests["items"][0]["queue_ids"] == [res_json.get("uuid")]


def test_create_while_event_pending_exists_but_allowing_multiple():
    payload = {
        "data_id": str(uuid4()),
        "name": "some_name",
        "label": "some_label",
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
        "payload": default_payload,
        "allow_multiple": True,
    }
    # create an already pending event
    correlation_id = str(uuid4())
    dao.create(
        uuid=correlation_id,
        data_id=payload.get("data_id"),
        name=payload.get("name"),
        label="some_other_label",
        correlation_id=correlation_id,
        kind=KindEnum.AWS_LAMBDA,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.post(
        "/events",
        json=payload,
    )

    expected_res = {
        **payload,
        "status": StatusEnum.PENDING,
        "version": "v_0",
    }

    res_json = res.json()
    del res_json["payload_file_name"]
    del expected_res["payload"]

    assert res.status_code == 201
    assert expected_res.items() < res_json.items()


def test_create_while_event_error_exists_and_enqueue_on_previous_error_is_true():
    payload = {
        "data_id": str(uuid4()),
        "name": "some_name",
        "label": "some_label",
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
        "payload": default_payload,
        "allow_multiple": False,
        "enqueue_on_previous_error": True,
    }
    uuid = str(uuid4())

    # create similar event with error status
    dao.create(
        uuid=uuid,
        data_id=payload.get("data_id"),
        name=payload.get("name"),
        label="some_other_label",
        correlation_id=uuid,
        kind=KindEnum.AWS_LAMBDA,
        status=StatusEnum.ERROR,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.post(
        "/events",
        json=payload,
    )

    expected_res = {
        **payload,
        "status": StatusEnum.QUEUED,
        "version": "v_0",
    }

    res_json = res.json()
    del res_json["payload_file_name"]
    del expected_res["payload"]
    event = dao.query(uuid)["items"][0]

    assert res.status_code == 201
    assert expected_res.items() < res_json.items()
    assert event["queue_ids"] == [res_json.get("uuid")]
    assert event["status"] == StatusEnum.ERROR


def test_create_while_event_error_exists_and_enqueue_on_previous_error_is_false():
    payload = {
        "data_id": str(uuid4()),
        "name": "some_name",
        "label": "some_label",
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
        "payload": default_payload,
        "allow_multiple": False,
        "enqueue_on_previous_error": False,
    }
    uuid = str(uuid4())

    # create similar event with error status
    dao.create(
        uuid=uuid,
        data_id=payload.get("data_id"),
        name=payload.get("name"),
        label="some_other_label",
        correlation_id=uuid,
        kind=KindEnum.AWS_LAMBDA,
        status=StatusEnum.ERROR,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.post(
        "/events",
        json=payload,
    )

    expected_res = {
        **payload,
        "status": StatusEnum.PENDING,
        "version": "v_0",
    }

    res_json = res.json()
    del res_json["payload_file_name"]
    del expected_res["payload"]
    error_event = dao.query(uuid)["items"][0]
    # children_error_event = dao.query(children_uuid)["items"][0]

    assert res.status_code == 201
    assert expected_res.items() < res_json.items()
    assert error_event["queue_ids"] == []
    assert error_event["status"] == StatusEnum.IGNORED
    # assert children_error_event["status"] == StatusEnum.IGNORED


def test_create_while_event_pending_exists_and_ignore_on_previous_active_is_true():
    payload = {
        "data_id": str(uuid4()),
        "name": "some_name",
        "label": "some_label",
        "kind": KindEnum.AWS_LAMBDA,
        "resource_config": default_resource_config,
        "payload": default_payload,
        "allow_multiple": False,
        "ignore_on_previous_active": True,
    }
    uuid = str(uuid4())

    # create similar event with error status
    dao.create(
        uuid=uuid,
        data_id=payload.get("data_id"),
        name=payload.get("name"),
        label="some_other_label",
        correlation_id=uuid,
        kind=KindEnum.AWS_LAMBDA,
        status=StatusEnum.PENDING,
        resource_config=default_resource_config,
        payload_file_name=str(uuid4()),
    )

    res = client.post(
        "/events",
        json=payload,
    )

    expected_res = {
        **payload,
        "status": StatusEnum.IGNORED,
        "version": "v_0",
    }

    res_json = res.json()
    del res_json["payload_file_name"]
    del expected_res["payload"]
    event = dao.query(uuid)["items"][0]

    assert res.status_code == 201
    assert expected_res.items() < res_json.items()
    assert event["queue_ids"] == []
