from uuid import uuid4
from unittest.mock import patch

import pytest

from . import default_payload
from ..utils import db


@pytest.fixture(scope="session", autouse=True)
def default_session_fixture():
    with patch("src.service.storage.get_event_payload", return_value=default_payload):
        with patch(
            "src.service.storage.store_event_payload", return_value=str(uuid4())
        ):
            yield


@pytest.fixture(autouse=True)
def init():
    db.reset_db()
