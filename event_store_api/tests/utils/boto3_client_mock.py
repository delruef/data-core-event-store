import json


class Boto3ClientMock:  # pylint: disable=too-few-public-methods
    def __init__(self, config, resource_config, payload, invoke_return_value, *, assert_payload=True):
        self.resource_config = resource_config
        self.payload = payload
        self.invoke_return_value = invoke_return_value
        self.config = config
        self.assert_payload = assert_payload

    def invoke(
        self, FunctionName, InvocationType, Payload
    ):  # pylint: disable=invalid-name
        expected_function_name = self.resource_config.get("function_name")

        if (self.assert_payload):
            assert json.loads(Payload).items() <= self.payload.items()

        assert FunctionName == expected_function_name
        assert InvocationType == "Event"
        return self.invoke_return_value

    def assume_role(
        self, RoleArn, RoleSessionName
    ):  # pylint: disable=invalid-name,no-self-use
        assert RoleArn == self.config.LAMBDA_ROLES[2]
        assert RoleSessionName == self.config.ROLE_SESSION_NAME

        return {
            "Credentials": {
                "AccessKeyId": "AccessKeyId",
                "SecretAccessKey": "SecretAccessKey",
                "SessionToken": "SessionToken",
            }
        }
