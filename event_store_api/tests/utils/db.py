from src.models.event_store_model import EventStore

models = [EventStore]


def reset_db():
    for model in models:
        if model.exists():
            model.delete_table()

    for model in models:
        if not model.exists():
            model.create_table(wait=True)
