from datetime import datetime, timezone, timedelta

from pydash import map_, find

from .config import PaginationEnum, get_config
from .models.event_store_model import EventStore

config = get_config()


def get_one_by_id(uuid, version="v_0"):
    try:
        event_store = EventStore.get(uuid, version, consistent_read=True)
        return event_store.to_dict()
    except EventStore.DoesNotExist:
        return None


def paginate_result(models, limit):
    items = map_(models, lambda item: item.to_dict())

    return {
        "last_evaluated_key": models.last_evaluated_key,
        "limit": limit,
        "items": items,
    }


def query_by_version_index(
    hash_key="v_0",
    *,
    range_key_condition=None,
    filter_condition=None,
    limit=PaginationEnum.DEFAULT_LIMIT,
    last_evaluated_key=None,
    scan_index_forward=False,
):
    events = EventStore.version_index.query(
        hash_key=hash_key,
        range_key_condition=range_key_condition,
        filter_condition=filter_condition,
        limit=limit,
        last_evaluated_key=last_evaluated_key,
        scan_index_forward=scan_index_forward,
    )

    return paginate_result(events, limit)


def query_by_name_index(
    hash_key,
    *,
    range_key_condition=None,
    filter_condition=EventStore.version == "v_0",
    limit=PaginationEnum.DEFAULT_LIMIT,
    last_evaluated_key=None,
    scan_index_forward=False,
):

    events = EventStore.name_index.query(
        hash_key=hash_key,
        range_key_condition=range_key_condition,
        filter_condition=filter_condition,
        limit=limit,
        last_evaluated_key=last_evaluated_key,
        scan_index_forward=scan_index_forward,
    )

    return paginate_result(events, limit)


def query_by_label_index(
    hash_key,
    *,
    range_key_condition=None,
    filter_condition=EventStore.version == "v_0",
    limit=PaginationEnum.DEFAULT_LIMIT,
    last_evaluated_key=None,
    scan_index_forward=False,
):

    events = EventStore.label_index.query(
        hash_key=hash_key,
        range_key_condition=range_key_condition,
        filter_condition=filter_condition,
        limit=limit,
        last_evaluated_key=last_evaluated_key,
        scan_index_forward=scan_index_forward,
    )

    return paginate_result(events, limit)


def query_by_name_data_id_index(
    hash_key,
    *,
    range_key_condition=None,
    filter_condition=EventStore.version == "v_0",
    limit=PaginationEnum.DEFAULT_LIMIT,
    last_evaluated_key=None,
    scan_index_forward=False,
):

    events = EventStore.name_data_id_index.query(
        hash_key=hash_key,
        range_key_condition=range_key_condition,
        filter_condition=filter_condition,
        limit=limit,
        last_evaluated_key=last_evaluated_key,
        scan_index_forward=scan_index_forward,
    )

    return paginate_result(events, limit)


def get_many_by_name_or_name_data_id_index(
    name,
    *,
    label=None,
    data_id=None,
    version="v_0",
    status=None,
    **pagination,
):
    filter_condition = None

    if version:
        filter_condition &= EventStore.version == version

    if status:
        filter_condition &= EventStore.status == status

    if label:
        filter_condition &= EventStore.label == label

    if bool(data_id):
        return query_by_name_data_id_index(
            f"{name}#{data_id}",
            filter_condition=filter_condition,
            **pagination,
        )

    return query_by_name_index(name, filter_condition=filter_condition, **pagination)


def get_many_by_label_index(
    label,
    *,
    data_id=None,
    version="v_0",
    status=None,
    **pagination,
):
    filter_condition = None

    if version:
        filter_condition &= EventStore.version == version

    if status:
        filter_condition &= EventStore.status == status

    if data_id:
        filter_condition &= EventStore.data_id == data_id

    return query_by_label_index(label, filter_condition=filter_condition, **pagination)


def get_many_by_version_index(version="v_0", *, status, data_id, **pagination):
    filter_condition = None

    if status:
        filter_condition &= EventStore.status == status

    if data_id:
        filter_condition &= EventStore.data_id == data_id

    return query_by_version_index(
        version, filter_condition=filter_condition, **pagination
    )


def query(
    hash_key,
    *,
    range_key_condition=EventStore.version == "v_0",
    filter_condition=None,
    limit=PaginationEnum.DEFAULT_LIMIT,
    last_evaluated_key=None,
    scan_index_forward=True,
):

    events = EventStore.query(
        hash_key=hash_key,
        range_key_condition=range_key_condition,
        filter_condition=filter_condition,
        limit=limit,
        last_evaluated_key=last_evaluated_key,
        scan_index_forward=scan_index_forward,
    )

    return paginate_result(events, limit)


def batch_get_many(filters):
    return map_(list(EventStore.batch_get(filters)), lambda event: event.to_dict())


def get_many_by_uuids(uuids):
    filters = map_(
        uuids,
        lambda uuid: (uuid, "v_0"),
    )
    return batch_get_many(filters)


def get_many_by_correlation_id(correlation_id):
    event_stores = list(
        EventStore.correlation_id_index.query(
            correlation_id, EventStore.version == "v_0"
        )
    )

    return map_(
        event_stores,
        lambda item: item.to_dict(),
    )


def get_last_related_event(
    name, *, data_id=None, exclude_ids=None, statuses=None, delta_min_updated_at=None
):

    if delta_min_updated_at:
        # do not look for entries having updated_at property
        # less than (now - delta_min_updated_at)
        delta_minutes = timedelta(minutes=delta_min_updated_at)
        range_key_filter = (
            EventStore.updated_at > datetime.now(timezone.utc) - delta_minutes
        )
    else:
        range_key_filter = None

    filters = EventStore.version == "v_0"

    if statuses:
        filters = filters & (EventStore.status.is_in(*statuses))

    if exclude_ids:
        for exclude_id in exclude_ids:
            filters = filters & ~(EventStore.uuid.contains(exclude_id))

    if data_id:
        events = EventStore.name_data_id_index.query(
            f"{name}#{data_id}",
            range_key_filter,
            filters,
            scan_index_forward=False,
        )
    else:
        events = EventStore.name_index.query(
            name,
            range_key_filter,
            filters,
            scan_index_forward=False,
        )

    event = next(events, None)

    return event.to_dict() if event else None


def create_calculated_fields(event):
    if hasattr(event, "name") and event.name:
        data_id = event.data_id if hasattr(event, "data_id") else None
        event.name_data_id = event.name if not data_id else f"{event.name}#{data_id}"


def create(**payload):
    event_store = EventStore(**payload)
    create_calculated_fields(event_store)
    event_store.save()

    return event_store.to_dict()


def update_status(status, message=None):
    actions = []
    if message:
        actions = [EventStore.message.set(message)]

    return [*actions, EventStore.status.set(status)]


def update_payload_file_name(payload_file_name):
    if payload_file_name:
        return [EventStore.payload_file_name.set(payload_file_name)]

    return []


def update_queue_ids(queue_ids_payload):

    actions = []

    if isinstance(queue_ids_payload, list):
        actions = [EventStore.queue_ids.set(queue_ids_payload)]
    else:
        items_to_add = queue_ids_payload.get("add", [])

        if items_to_add:
            actions = [
                EventStore.queue_ids.set(
                    (EventStore.queue_ids | []).append(items_to_add)
                )
            ]

    return actions


def execute_update_pipe(uuid, *actions):
    EventStore(uuid=uuid).update(
        actions=[*actions, EventStore.updated_at.set(datetime.now(timezone.utc))]
    )

    return get_one_by_id(uuid)


def get_number_of_versions(uuid):
    return EventStore.count(
        uuid,
    )


def clone_events_and_set_last_version(given_events, db_events):
    with EventStore.batch_write() as batch:
        for db_event in db_events:
            event = find(
                given_events,
                lambda item: item.get("uuid")
                == db_event.uuid,  # pylint: disable=cell-var-from-loop
            )

            if event:
                num_versions = get_number_of_versions(db_event.uuid)
                new_version = f"v_{num_versions}"
                copy_payload = {
                    **db_event.to_dict(),
                    "version": new_version,
                }
                copy_payload["created_at"] = datetime.fromisoformat(
                    copy_payload["created_at"]
                )
                copy_payload["updated_at"] = datetime.fromisoformat(
                    copy_payload["updated_at"]
                )

                event_store_copy = EventStore(**copy_payload)
                create_calculated_fields(event_store_copy)

                batch.save(event_store_copy)


def version_event_stores(events, delete_current_version=True):
    db_events = []

    for db_event in EventStore.batch_get(
        map_(events, lambda ev: (ev.get("uuid"), "v_0"))
    ):
        db_events.append(db_event)

    clone_events_and_set_last_version(events, db_events)

    if delete_current_version:
        with EventStore.batch_write() as batch:
            for db_event in db_events:
                event = find(
                    events,
                    lambda item: item.get("uuid")
                    == db_event.uuid,  # pylint: disable=cell-var-from-loop
                )

                if event:
                    batch.delete(db_event)
