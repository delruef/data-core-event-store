import typing as t
import os
from functools import lru_cache
from enum import Enum, IntEnum

from pydantic import BaseSettings, Field, validator


class FileSystemEnum(str, Enum):
    LOCAL = "local"
    S3 = "s3"


class EventStoreConfig(BaseSettings):  # pylint: disable=too-few-public-methods
    DYNAMODB_TABLE_NAME: str
    DYNAMODB_REGION: str
    DYNAMODB_HOST: t.Optional[str]
    # https://github.com/samuelcolvin/pydantic/issues/1458
    LAMBDA_ROLES: t.Union[str, t.List[str]] = Field(..., env="LAMBDA_ROLES")
    SPEC_VERSION: float = 2.0
    ROLE_SESSION_NAME: str = "cross_acct_lambda"
    APP_NAMES: t.List[str] = ["api-interfaces", "pegaz", "dataex"]
    STORAGE_FILE_SYSTEM: t.Optional[str] = Field(
        default=FileSystemEnum.LOCAL, env="STORAGE_FILE_SYSTEM"
    )
    STORAGE_PATH: t.Optional[str] = Field(
        default=f"{os.getcwd()}/storage", env="STORAGE_PATH"
    )

    # https://github.com/samuelcolvin/pydantic/issues/1458
    @validator("LAMBDA_ROLES", pre=True)
    def _assemble_lambda_roles(
        cls, lambda_roles
    ):  # pylint: disable=no-self-argument,no-self-use
        if isinstance(lambda_roles, str):
            return [item.strip() for item in lambda_roles.split(",")]
        return lambda_roles

    class Config:  # pylint: disable=too-few-public-methods
        env_file = f".env.{os.environ.get('ENV', 'development')}"


@lru_cache()
def get_config():
    return EventStoreConfig()


class PaginationEnum(IntEnum):
    DEFAULT_LIMIT = 25
    MAX_LIMIT = 100


class StatusEnum(str, Enum):
    ACCEPTED = "accepted"
    QUEUED = "queued"
    PENDING = "pending"
    ERROR = "error"
    FINISHED = "finished"
    IGNORED = "ignored"


class EventCreateStatusEnum(str, Enum):
    ACCEPTED = "accepted"
    PENDING = "pending"


class KindEnum(str, Enum):
    AWS_LAMBDA = "aws_lambda"
    API_CALL = "api_call"


class MethodsEnum(str, Enum):
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    PATCH = "PATCH"


class ErrorsEnum(str, Enum):
    NOT_FOUND = {
        "code": "event_store_not_found",
        "messasge": "An event having the given id was not found",
    }

    PARENT_NOT_FOUND = {
        "code": "event_store_parent_not_found",
        "messasge": "A parent having the given id was not found",
    }

    EVENT_IS_NEITHER_ACCEPTED_NOR_PENDING = {
        "code": "event_store_event_is_neither_accepted_nor_pending",
        "messasge": "Cannot update status for event wich status is neither accepted nor pending",
    }

    ACTIVE_EVENT_FOUND = {
        "code": "event_store_active_event_found",
        "messasge": """Cannot rerun an event having a pending,
        accepted or queued status, or associated to an event having a pending,
        accepted or queued status""",
    }

    NO_KIND_FOUND = {
        "code": "event_store_no_kind_found",
        "messasge": "No action associated to model kind",
    }

    NO_CORRESPONDING_ROLE = {
        "code": "event_store_no_corresponding_role",
        "messasge": "No crossaccount execution role associated to the lambda arn found",
    }

    CANNOT_EDIT_STOPPED_EVENT_QUEUE_IDS = {
        "code": "event_store_cannot_edit_stopped_event_queue_ids",
        "messasge": "No crossaccount execution role associated to the lambda arn found",
    }
