from functools import reduce
from uuid import uuid4
import json
from inspect import iscoroutinefunction

from fastapi import HTTPException
from pydash import get, map_, find
import boto3
import aiohttp

from . import dao
from .helpers import get_resource_payload, storage
from .config import StatusEnum, ErrorsEnum, KindEnum, get_config
from .schemas.event_store_schema import (
    EventStoreEditSchema,
)

pending_events = {}
config = get_config()


def get_one_or_fail(uuid):
    event_store = dao.get_one_by_id(str(uuid))
    if not event_store:
        raise HTTPException(status_code=422, detail=ErrorsEnum.NOT_FOUND)

    return event_store


def get_parent_or_fail(parent_id):
    parent = dao.get_one_by_id(str(parent_id))
    if not parent:
        raise HTTPException(status_code=422, detail=ErrorsEnum.PARENT_NOT_FOUND)

    return parent


def get_event_uuid_and_correlation_id(event_payload):
    uuid = event_payload.get("uuid") or str(uuid4())
    if event_payload.get("parent_id"):
        parent = get_parent_or_fail(event_payload.get("parent_id"))
        # TODO is parent at least in pending status ?
        return {"uuid": uuid, "correlation_id": parent.get("correlation_id")}

    return {
        "uuid": uuid,
        "correlation_id": uuid,
    }


def get_many(*, filters, **pagination):
    name = filters.get("name")
    label = filters.get("label")
    data_id = filters.get("data_id")
    version = filters.get("version")
    status = filters.get("status")

    if bool(name):
        return dao.get_many_by_name_or_name_data_id_index(
            name,
            label=label,
            data_id=data_id,
            version=version,
            status=status,
            **pagination,
        )

    if bool(label):
        return dao.get_many_by_label_index(
            label,
            data_id=data_id,
            version=version,
            status=status,
            **pagination,
        )

    return dao.get_many_by_version_index(
        version, status=status, data_id=data_id, **pagination
    )


async def create_or_update(event_payload_orig):
    payload = get_resource_payload(event_payload_orig.get("payload"))

    event_payload = event_payload_orig.copy()
    del event_payload["payload"]

    event_payload = {
        **event_payload,
        **get_event_uuid_and_correlation_id(event_payload),
    }
    event_payload["payload_file_name"] = storage.store_event_payload(payload)
    enqueue_on_previous_error = event_payload.get("enqueue_on_previous_error")
    ignore_on_previous_active = event_payload.get("ignore_on_previous_active")

    dao.create(**{**event_payload, "status": StatusEnum.ACCEPTED})
    allow_multiple = event_payload.get("allow_multiple")
    data_id = event_payload.get("data_id")
    status = event_payload.get("status")

    related_event = None

    if data_id and not allow_multiple and status == StatusEnum.PENDING:
        # the last event having same name, or name and data_id
        related_event = dao.get_last_related_event(
            event_payload.get("name"),
            data_id=data_id,
            exclude_ids=[
                *event_payload.get("queue_ids", []),
                event_payload.get("uuid"),
            ],
            statuses=[
                StatusEnum.PENDING,
                StatusEnum.ACCEPTED,
                StatusEnum.ERROR,
            ],
            # avoid to check for events older than 15 minutes
            # this param should be deleted after the frontend implementation
            # for the event store api
            delta_min_updated_at=15,
        )

        # if enqueue_on_previous_error is False and the last related event is in error
        # we ignore the last related error and force event execution
        # the last related event in error will be updated to ignored
        if (
            not enqueue_on_previous_error
            and get(related_event, "status") == StatusEnum.ERROR
        ):
            await edit(
                related_event["uuid"], EventStoreEditSchema(status=StatusEnum.IGNORED)
            )
            related_event = None

        # if ignore_on_previous_active is True and the last related event
        # is in pending or accepted status, we ignore the event execution
        elif ignore_on_previous_active and get(related_event, "status") in [
            StatusEnum.ACCEPTED,
            StatusEnum.PENDING,
        ]:
            await edit(
                event_payload.get("uuid"),
                EventStoreEditSchema(status=StatusEnum.IGNORED),
            )
            related_event = None
            status = StatusEnum.IGNORED

        elif bool(related_event):
            status = StatusEnum.QUEUED

    updated_event = dao.execute_update_pipe(
        event_payload.get("uuid"), *dao.update_status(status)
    )

    # the event id will be stored on the queuue of the similar event root item
    # it'll be processed after the execution of the last event associated to it
    if bool(related_event) and status == StatusEnum.QUEUED:
        enqueue_event(updated_event.get("uuid"), related_event.get("uuid"))

    return updated_event


async def create(schema_payload):
    return await create_or_update(schema_payload.to_dict())


async def update(schema_payload, event_uuid):
    event = get_one_or_fail(event_uuid)

    events = get_event_store_with_nested_children(event)

    active_events = filter_active_events(events, exclude_ids=[event.get("uuid")])

    # the event is updatable (moving to pending status) only if it's actual status
    # is accepted, error or finished
    # Otherwise it is already in execution or it will while dequeueing
    # Also the child events can't be in accepted, queued or pending status
    if active_events or event.get("status") != StatusEnum.ACCEPTED:
        raise HTTPException(status_code=422, detail=ErrorsEnum.ACTIVE_EVENT_FOUND)

    dao.version_event_stores(events)

    event_payload = schema_payload.to_dict()

    event_payload["uuid"] = str(event_uuid)
    event_payload["status"] = StatusEnum.PENDING
    event_payload["queue_ids"] = event.get("queue_ids", [])

    return await create_or_update(event_payload)


async def run_event_id_and_add_queue(event_uuid, queue_ids):
    event = dao.get_one_by_id(event_uuid)
    payload = storage.get_event_payload(event)
    actions = dao.update_queue_ids({"add": queue_ids})
    await run(event, payload, actions)


async def run_enqueued_events_and_reset_queue(event):
    queue_ids = event.get("queue_ids", []).copy()

    if len(queue_ids) == 0:
        return event

    # we retrive the first event in the event queue
    first_queued_event = queue_ids.pop(0)

    # we run the first event in the event queue and add the other queued events in its queue
    await run_event_id_and_add_queue(first_queued_event, queue_ids)

    # we reset the current event queue
    actions = dao.update_queue_ids([])
    return dao.execute_update_pipe(event.get("uuid"), *actions)


def add_update_status_pipe(event, schema_payload):
    if not hasattr(schema_payload, "status") or not schema_payload.status:
        return []

    # status cannot be updated to finished or error if its value
    # wasn't first accepted or pending
    if schema_payload.status in [StatusEnum.FINISHED, StatusEnum.ERROR] and event.get(
        "status"
    ) not in [StatusEnum.ACCEPTED, StatusEnum.PENDING]:
        raise HTTPException(
            status_code=422, detail=ErrorsEnum.EVENT_IS_NEITHER_ACCEPTED_NOR_PENDING
        )

    return dao.update_status(schema_payload.status, schema_payload.message)


def add_update_payload_pipe(payload):
    if payload:
        payload_file_name = storage.store_event_payload(payload)
        return dao.update_payload_file_name(payload_file_name)

    return []


def add_update_queue_ids_pipe(event, schema_payload):
    if not hasattr(schema_payload, "queue_ids") or not schema_payload.queue_ids:
        return []

    if event.get("status") in [
        StatusEnum.FINISHED,
        StatusEnum.ERROR,
        StatusEnum.IGNORED,
    ]:
        raise HTTPException(
            status_code=422, detail=ErrorsEnum.CANNOT_EDIT_STOPPED_EVENT_QUEUE_IDS
        )

    if isinstance(schema_payload.queue_ids, list):
        payload = map_(schema_payload.queue_ids, str)
    else:
        payload = {
            "add": (
                map_(schema_payload.queue_ids.add, str)
                if hasattr(schema_payload.queue_ids, "add")
                else []
            )
        }

    return dao.update_queue_ids(payload)


async def edit(uuid, schema_payload):
    event = get_one_or_fail(uuid)
    actions = [
        *add_update_status_pipe(event, schema_payload),
        *add_update_queue_ids_pipe(event, schema_payload),
    ]

    if len(actions) == 0:
        return event

    dao.version_event_stores([event], delete_current_version=False)

    updated_event = dao.execute_update_pipe(str(uuid), *actions)

    if hasattr(schema_payload, "status") and schema_payload.status in [
        StatusEnum.FINISHED,
        StatusEnum.IGNORED,
    ]:
        return await run_enqueued_events_and_reset_queue(updated_event)

    return updated_event


def enqueue_event(event_id, similar_active_event_id):
    return dao.execute_update_pipe(
        similar_active_event_id, *dao.update_queue_ids({"add": [event_id]})
    )


def get_event_store_direct_children(event_stores, parent_event_store):
    return filter(
        lambda es: es.get("parent_id") == parent_event_store.get("uuid"),
        event_stores,
    )


def get_event_stores_nested_children(event_stores, parent_event_stores):
    children = reduce(
        lambda current_children, parent_event_store: [
            *current_children,
            *get_event_store_direct_children(event_stores, parent_event_store),
        ],
        parent_event_stores,
        [],
    )

    if not children:
        return []

    return [
        *children,
        *get_event_stores_nested_children(event_stores, children),
    ]


def get_event_store_with_nested_children(event_store):
    event_stores_by_correlation_id = dao.get_many_by_correlation_id(
        event_store.get("correlation_id")
    )
    populated_event_store = find(
        event_stores_by_correlation_id,
        lambda es: es.get("uuid") == event_store.get("uuid"),
    )

    return [
        populated_event_store,
        *get_event_stores_nested_children(
            event_stores_by_correlation_id, [event_store]
        ),
    ]


def has_role_account_and_app_name(role, account, app_name):
    role_infos = role.split(":")
    role_account = role_infos[4]
    role_app_name = find(config.APP_NAMES, lambda app_name: app_name in role_infos[5])

    return account == role_account and app_name == role_app_name


def find_invoker_role(function_arn):
    lambda_infos = function_arn.split(":")
    lambda_account = lambda_infos[4]
    lambda_app_name = find(
        config.APP_NAMES, lambda app_name: app_name in lambda_infos[6]
    )
    return find(
        config.LAMBDA_ROLES,
        lambda role: has_role_account_and_app_name(
            role, lambda_account, lambda_app_name
        ),
    )


def run_aws_lambda(event, payload):
    uuid = event.get("uuid")
    resource_config = event.get("resource_config")
    function_name = resource_config.get("function_name")
    payload = {**payload, "eventId": uuid}
    invoker_role = find_invoker_role(function_name)

    if not invoker_role:
        raise HTTPException(status_code=422, detail=ErrorsEnum.NO_CORRESPONDING_ROLE)

    sts_connection = boto3.client("sts")
    lambda_target_account = sts_connection.assume_role(
        RoleArn=invoker_role,
        RoleSessionName=config.ROLE_SESSION_NAME,
    )

    access_key = lambda_target_account["Credentials"]["AccessKeyId"]
    secret_key = lambda_target_account["Credentials"]["SecretAccessKey"]
    session_token = lambda_target_account["Credentials"]["SessionToken"]

    # create service client using the assumed role credentials, e.g. S3
    client = boto3.client(
        "lambda",
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        aws_session_token=session_token,
    )

    return client.invoke(
        FunctionName=function_name,
        InvocationType="Event",
        Payload=json.dumps(payload),
    )


async def run_api_call(event, payload):
    uuid = event.get("uuid")
    resource_config = event.get("resource_config")

    method = resource_config.get("method")
    url = resource_config.get("url")

    headers = {**payload.get("headers", {}), "X-Event-ID": uuid}
    params = payload.get("query_params")
    data = payload.get("body")

    try:
        async with aiohttp.ClientSession() as session:
            await session.request(
                method,
                url,
                headers=headers,
                params=params,
                data=data,
                timeout=1,
                raise_for_status=False,
            )
    except Exception:  # pylint: disable=broad-except
        pass


def filter_active_events(events, exclude_ids=None):
    return find(
        events,
        lambda es: es.get("status")
        in [StatusEnum.PENDING, StatusEnum.ACCEPTED, StatusEnum.QUEUED]
        and (not exclude_ids or es.get("uuid") not in exclude_ids),
    )


async def run(event, payload, actions=None):
    kinds_to_run = {
        KindEnum.AWS_LAMBDA: run_aws_lambda,
        KindEnum.API_CALL: run_api_call,
    }
    kind = event.get("kind")
    if not kinds_to_run[kind]:
        raise HTTPException(status_code=422, detail=ErrorsEnum.NO_KIND_FOUND)

    dao.version_event_stores([event], delete_current_version=False)
    dao.execute_update_pipe(
        event.get("uuid"),
        *dao.update_status(StatusEnum.ACCEPTED),
        *add_update_payload_pipe(payload),
        *(actions or []),
    )

    return (
        await kinds_to_run[kind](event, payload)
        if iscoroutinefunction(kinds_to_run[kind])
        else kinds_to_run[kind](event, payload)
    )


async def rerun(uuid, rerun_schema_payload):
    event = get_one_or_fail(uuid)
    events = get_event_store_with_nested_children(event)

    active_events = filter_active_events(events)

    if active_events:
        raise HTTPException(status_code=422, detail=ErrorsEnum.ACTIVE_EVENT_FOUND)

    payload = (
        get_resource_payload(rerun_schema_payload.payload)
        if rerun_schema_payload.payload
        else storage.get_event_payload(event)
    )

    return await run(event, payload)
