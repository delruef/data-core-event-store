import typing as t
from uuid import UUID
import json

from fastapi import FastAPI, Query, status as http_status, Depends
from mangum import Mangum

from .config import PaginationEnum
from .schemas.event_store_schema import (
    EventStoreSchema,
    EventStoreCreateSchema,
    EventStoreEditSchema,
    EventStoreRerunSchema,
    EventStorePaginatedResponseSchema,
    EventStoreUpdateSchema,
    EventStoreGetManySchema,
)

from . import service

app = FastAPI()


def compute_get_many_by_name_options(
    limit: t.Optional[int] = Query(
        PaginationEnum.DEFAULT_LIMIT, ge=1, le=PaginationEnum.MAX_LIMIT
    ),
    last_evaluated_key: t.Optional[str] = None,
    name: t.Optional[str] = None,
    label: t.Optional[str] = None,
    data_id: t.Optional[str] = None,
    status: t.Optional[str] = None,
    version: str = Query("v_0", regex=r"^v_\d+$"),
):  # pylint: disable=too-many-arguments

    last_evaluated_key = json.loads(last_evaluated_key) if last_evaluated_key else None

    EventStoreGetManySchema(
        last_evaluated_key=last_evaluated_key,
        name=name,
        label=label,
        data_id=data_id,
        status=status,
        version=version,
    )

    return {
        "limit": limit,
        "last_evaluated_key": last_evaluated_key,
        "filters": {
            "name": name,
            "label": label,
            "data_id": data_id,
            "status": status,
            "version": version,
        },
    }


@app.get(
    "/events/{uuid}",
    status_code=http_status.HTTP_200_OK,
    response_model=EventStoreSchema,
)
async def get_one(uuid: UUID):
    return service.get_one_or_fail(uuid)


@app.get("/events", response_model=EventStorePaginatedResponseSchema)
async def get_many(options=Depends(compute_get_many_by_name_options)):
    return service.get_many(**options)


@app.post(
    "/events", status_code=http_status.HTTP_201_CREATED, response_model=EventStoreSchema
)
async def create(schema_payload: EventStoreCreateSchema):
    return await service.create(schema_payload)


@app.put(
    "/events/{uuid}",
    status_code=http_status.HTTP_200_OK,
    response_model=EventStoreSchema,
)
async def update(uuid: UUID, schema_payload: EventStoreUpdateSchema):
    return await service.update(schema_payload, uuid)


@app.patch("/events/{uuid}", response_model=EventStoreSchema)
async def edit(uuid: UUID, schema_payload: EventStoreEditSchema):
    return await service.edit(uuid, schema_payload)


@app.post("/events/{uuid}/rerun", status_code=http_status.HTTP_204_NO_CONTENT)
async def rerun(
    uuid: UUID,
    rerun_schema_payload: t.Optional[EventStoreRerunSchema] = EventStoreRerunSchema(),
):
    await service.rerun(uuid, rerun_schema_payload)
    return {}


handler = Mangum(app, log_level="error")
