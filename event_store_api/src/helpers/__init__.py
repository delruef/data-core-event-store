from inspect import isclass


def get_resource_payload(payload):
    return payload.to_dict() if isclass(payload) else payload
