import json
import io
from uuid import uuid4

import boto3

from ..config import get_config, FileSystemEnum

s3_client = boto3.client("s3")
config = get_config()


def local_get_event_payload(event):
    file_path = f"{config.STORAGE_PATH}/{event.get('payload_file_name')}"

    with open(file_path) as file:
        return json.loads(file.read())


def s3_get_event_payload(event):
    s3_response_object = s3_client.get_object(
        Bucket=config.STORAGE_PATH, Key=event.get("payload_file_name")
    )
    return json.loads(s3_response_object["Body"].read().decode("utf-8"))


def local_store_event_payload(payload):
    file_name = str(uuid4())
    file_path = f"{config.STORAGE_PATH}/{file_name}"
    with open(file_path, "w") as file:
        file.write(json.dumps(payload))

    return file_name


def s3_store_event_payload(payload):
    file_name = str(uuid4())
    s3_client.upload_fileobj(
        io.BytesIO(json.dumps(payload).encode()), config.STORAGE_PATH, file_name
    )
    return file_name


def get_event_payload(event):
    defs = {
        FileSystemEnum.LOCAL: local_get_event_payload,
        FileSystemEnum.S3: s3_get_event_payload,
    }

    return defs[config.STORAGE_FILE_SYSTEM](event)


def store_event_payload(payload):
    defs = {
        FileSystemEnum.LOCAL: local_store_event_payload,
        FileSystemEnum.S3: s3_store_event_payload,
    }

    return defs[config.STORAGE_FILE_SYSTEM](payload)
