import orjson
from pydantic import BaseModel


def orjson_dumps(val, *, default):
    # orjson.dumps returns bytes, to match standard json.dumps we need to decode
    return orjson.dumps(val, default=default).decode()


class BaseSchema(BaseModel):  # pylint: disable=too-few-public-methods
    def to_dict(self):
        return orjson.loads(self.json())

    class Config:  # pylint: disable=too-few-public-methods
        json_loads = orjson.loads
        json_dumps = orjson_dumps
