import typing as t
from uuid import UUID
from datetime import datetime

from pydantic import Field, validator

from ..config import (
    PaginationEnum,
    StatusEnum,
    EventCreateStatusEnum,
    KindEnum,
    MethodsEnum,
)
from .base_schema import BaseSchema


class AwsLambdaResourceConfig(BaseSchema):  # pylint: disable=too-few-public-methods
    function_name: str


class ApiCallResourceConfig(BaseSchema):  # pylint: disable=too-few-public-methods
    url: str
    method: MethodsEnum


AwsPayload = t.Dict[str, t.Any]


class ApiCallPayload(BaseSchema):
    headers: t.Optional[t.Dict[str, t.Any]]
    query_params: t.Optional[t.Dict[str, t.Any]]
    body: t.Optional[str]


KindToResourceConfig = {
    KindEnum.AWS_LAMBDA: AwsLambdaResourceConfig,
    KindEnum.API_CALL: ApiCallResourceConfig,
}
KindToPayload = {KindEnum.AWS_LAMBDA: AwsPayload, KindEnum.API_CALL: ApiCallPayload}

ResourceConfig = t.Union[AwsLambdaResourceConfig, ApiCallResourceConfig]
Payload = t.Union[AwsPayload, ApiCallPayload]


class EventStoreSchema(BaseSchema):  # pylint: disable=too-few-public-methods
    uuid: UUID
    version: str = Field(regex=r"^v_\d+$")
    status: StatusEnum
    message: t.Optional[str]
    correlation_id: UUID
    parent_id: t.Optional[UUID]
    data_id: t.Optional[str]
    name: str
    label: str
    kind: KindEnum
    created_at: datetime
    updated_at: datetime
    resource_config: t.Optional[ResourceConfig]
    allow_multiple: bool
    enqueue_on_previous_error: bool
    ignore_on_previous_active: bool
    spec_version: float
    queue_ids: t.Optional[t.List[UUID]]
    payload_file_name: str
    additional_infos: t.Optional[t.Dict[str, t.Any]]


class EventStoreCreateSchema(BaseSchema):  # pylint: disable=too-few-public-methods
    uuid: t.Optional[UUID]
    parent_id: t.Optional[UUID]
    data_id: t.Optional[str]
    status: t.Optional[EventCreateStatusEnum] = Field(StatusEnum.PENDING)
    name: str
    label: str
    kind: KindEnum
    resource_config: t.Optional[ResourceConfig]
    allow_multiple: t.Optional[bool] = Field(True)
    enqueue_on_previous_error: t.Optional[bool] = Field(False)
    ignore_on_previous_active: t.Optional[bool] = Field(False)
    payload: Payload
    additional_infos: t.Optional[t.Dict[str, t.Any]]

    @validator("resource_config")
    def resource_config_kind(
        cls, value, values
    ):  # pylint: disable=no-self-argument,no-self-use
        kind = values.get("kind")
        KindToResourceConfig[kind](**value.to_dict())

        return value

    @validator("payload")
    def payload_kind(
        cls, value, values
    ):  # pylint: disable=no-self-argument,no-self-use
        kind = values.get("kind")
        if isinstance(value, BaseSchema):
            KindToPayload[kind](**value.to_dict())

        return value

    # allow_multiple cannot be False if data_id is not present
    @validator("allow_multiple")
    def payload_allow_multiple(
        cls, value, values
    ):  # pylint: disable=no-self-argument,no-self-use
        allow_multiple = value
        data_id = values.get("data_id")

        if allow_multiple:
            return value

        assert bool(data_id) is True
        return value

    # enqueue_on_previous_error cannot be True if data_id is not present
    # or if allow_multiple is True
    @validator("enqueue_on_previous_error")
    def payload_enqueue_on_previous_error(
        cls, value, values
    ):  # pylint: disable=no-self-argument,no-self-use
        enqueue_on_previous_error = value
        allow_multiple = values.get("allow_multiple")
        data_id = values.get("data_id")

        if not bool(enqueue_on_previous_error):
            return value

        assert ((not bool(allow_multiple)) and bool(data_id)) is True

        return value

    # ignore_on_previous_active cannot be True if data_id is not present
    # or if allow_multiple is True or if enqueue_on_previous_error is True
    @validator("ignore_on_previous_active")
    def payload_ignore_on_previous_active(
        cls, value, values
    ):  # pylint: disable=no-self-argument,no-self-use
        ignore_on_previous_active = value
        allow_multiple = values.get("allow_multiple")
        enqueue_on_previous_error = values.get("enqueue_on_previous_error")
        data_id = values.get("data_id")

        if not bool(ignore_on_previous_active):
            return value

        assert (
            bool(allow_multiple) is not True
            and bool(data_id) is True
            and bool(enqueue_on_previous_error) is not True
        )

        return value


class EventStoreUpdateSchema(BaseSchema):  # pylint: disable=too-few-public-methods
    parent_id: t.Optional[UUID]
    data_id: t.Optional[str]
    name: str
    label: str
    kind: KindEnum
    resource_config: ResourceConfig
    allow_multiple: t.Optional[bool] = Field(True)
    enqueue_on_previous_error: t.Optional[bool] = Field(False)
    ignore_on_previous_active: t.Optional[bool] = Field(False)
    payload: Payload
    additional_infos: t.Optional[t.Dict[str, t.Any]]

    @validator("resource_config")
    def resource_config_kind(
        cls, value, values
    ):  # pylint: disable=no-self-argument,no-self-use
        kind = values.get("kind")
        KindToResourceConfig[kind](**value.to_dict())

        return value

    @validator("payload")
    def payload_kind(
        cls, value, values
    ):  # pylint: disable=no-self-argument,no-self-use
        kind = values.get("kind")
        if isinstance(value, BaseSchema):
            KindToPayload[kind](**value.to_dict())

        return value

    # allow_multiple cannot be False if data_id is not present
    @validator("allow_multiple")
    def payload_allow_multiple(
        cls, value, values
    ):  # pylint: disable=no-self-argument,no-self-use
        allow_multiple = value
        data_id = values.get("data_id")

        if allow_multiple:
            return value

        assert bool(data_id) is True
        return value

    # enqueue_on_previous_error cannot be True if data_id is not present
    # or if allow_multiple is True
    @validator("enqueue_on_previous_error")
    def payload_enqueue_on_previous_error(
        cls, value, values
    ):  # pylint: disable=no-self-argument,no-self-use
        enqueue_on_previous_error = value
        allow_multiple = values.get("allow_multiple")
        data_id = values.get("data_id")

        if not bool(enqueue_on_previous_error):
            return value

        assert ((not bool(allow_multiple)) and bool(data_id)) is True

        return value

    # ignore_on_previous_active cannot be True if data_id is not present
    # or if allow_multiple is True or if enqueue_on_previous_error is True
    @validator("ignore_on_previous_active")
    def payload_ignore_on_previous_active(
        cls, value, values
    ):  # pylint: disable=no-self-argument,no-self-use
        ignore_on_previous_active = value
        allow_multiple = values.get("allow_multiple")
        enqueue_on_previous_error = values.get("enqueue_on_previous_error")
        data_id = values.get("data_id")

        if not bool(ignore_on_previous_active):
            return value

        assert (
            bool(allow_multiple) is not True
            and bool(data_id) is True
            and bool(enqueue_on_previous_error) is not True
        )

        return value


class EventStoreQueueIdsAddSchema(BaseSchema):
    add: t.List[UUID]

    # add: t.Optional[t.List[UUID]]
    # remove: t.Optional[t.List[UUID]]

    # @validator("add")
    # def add_validator(
    #     cls, value, values
    # ):  # pylint: disable=no-self-argument,no-self-use
    #     remove = values.get("remove")
    #     # one between add or remove must be present
    #     assert bool(value or remove) is True

    #     return value


class EventStoreEditSchema(BaseSchema):  # pylint: disable=too-few-public-methods
    status: t.Optional[StatusEnum]
    message: t.Optional[str]
    queue_ids: t.Optional[t.Union[EventStoreQueueIdsAddSchema, t.List[UUID]]]

    @validator("message")
    def message_validator(
        cls, value, values
    ):  # pylint: disable=no-self-argument,no-self-use
        if not bool(value):
            return value

        status = values.get("status")
        assert bool(status) is True

        return value


class EventStoreRerunSchema(BaseSchema):  # pylint: disable=too-few-public-methods
    payload: t.Optional[Payload]


class EventStorePaginationId(BaseSchema):  # pylint: disable=too-few-public-methods
    S: UUID


class EventStorePaginationVersion(BaseSchema):  # pylint: disable=too-few-public-methods
    S: str = Field(regex=r"^v_\d+$")


class EventStorePaginationUpdatedAt(
    BaseSchema
):  # pylint: disable=too-few-public-methods
    S: datetime


class EventStorePaginationName(BaseSchema):  # pylint: disable=too-few-public-methods
    S: str


class EventStorePaginationLabel(BaseSchema):  # pylint: disable=too-few-public-methods
    S: str


class EventStorePaginationNameDataId(
    BaseSchema
):  # pylint: disable=too-few-public-methods
    S: str


class EventStoreGetManyByIdsLastEvaluatedKey(
    BaseSchema
):  # pylint: disable=too-few-public-methods
    uuid: EventStorePaginationId
    version: EventStorePaginationVersion
    updated_at: EventStorePaginationUpdatedAt


class EventStoreGetManyByNameLastEvaluatedKey(
    EventStoreGetManyByIdsLastEvaluatedKey
):  # pylint: disable=too-few-public-methods
    name: EventStorePaginationName


class EventStoreGetManyByLabelLastEvaluatedKey(
    EventStoreGetManyByIdsLastEvaluatedKey
):  # pylint: disable=too-few-public-methods
    label: EventStorePaginationLabel


class EventStoreGetManyByNameDataIdLastEvaluatedKey(
    EventStoreGetManyByIdsLastEvaluatedKey
):  # pylint: disable=too-few-public-methods
    name_data_id: EventStorePaginationNameDataId


EventStoreGetManyLastEvaluatedKey = t.Union[
    # order counts
    # https://github.com/tiangolo/fastapi/issues/2517
    EventStoreGetManyByNameDataIdLastEvaluatedKey,
    EventStoreGetManyByNameLastEvaluatedKey,
    EventStoreGetManyByLabelLastEvaluatedKey,
    EventStoreGetManyByIdsLastEvaluatedKey,
]


class EventStorePaginatedResponseSchema(
    BaseSchema
):  # pylint: disable=too-few-public-methods
    limit: int = Field(PaginationEnum.DEFAULT_LIMIT, ge=1, le=PaginationEnum.MAX_LIMIT)
    last_evaluated_key: t.Optional[EventStoreGetManyLastEvaluatedKey]
    items: t.List[EventStoreSchema]


class EventStoreGetManySchema(BaseSchema):
    last_evaluated_key: t.Optional[EventStoreGetManyLastEvaluatedKey]
    name: t.Optional[str]
    label: t.Optional[str]
    data_id: t.Optional[str]
    version: str = Field("v_0", regex=r"^v_\d+$")
    status: t.Optional[StatusEnum]

    @validator("last_evaluated_key")
    def last_evaluated_key_validator(
        cls, value, values
    ):  # pylint: disable=no-self-argument,no-self-use
        if not bool(value):
            return value

        name = values.get("name")
        data_id = values.get("name")
        label = values.get("label")

        if bool(name and data_id):
            EventStoreGetManyByNameDataIdLastEvaluatedKey(**value.to_dict())
        elif bool(name):
            EventStoreGetManyByNameLastEvaluatedKey(**value.to_dict())
        elif bool(label):
            EventStoreGetManyByLabelLastEvaluatedKey(**value.to_dict())
        else:
            EventStoreGetManyByIdsLastEvaluatedKey(**value.to_dict())

        return value
