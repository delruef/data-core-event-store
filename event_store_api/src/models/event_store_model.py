from datetime import datetime, timezone
from uuid import uuid4

from pynamodb.attributes import (
    UnicodeAttribute,
    JSONAttribute,
    BooleanAttribute,
    NumberAttribute,
    ListAttribute,
)
from pynamodb.indexes import GlobalSecondaryIndex, AllProjection
from pynamodb.constants import PAY_PER_REQUEST_BILLING_MODE

from .custom_attributes import UTCDateTimeAttributeISO
from .base_model import BaseModel
from ..config import get_config, StatusEnum

config = get_config()


class NameDataIdIndex(GlobalSecondaryIndex):
    """
    This class represents name and data_id global secondary index
    """

    class Meta:  # pylint: disable=too-few-public-methods
        read_capacity_units = 1
        write_capacity_units = 1
        # All attributes are projected
        projection = AllProjection()

    name_data_id = UnicodeAttribute(hash_key=True)
    updated_at = UTCDateTimeAttributeISO(range_key=True)


class NameIndex(GlobalSecondaryIndex):
    """
    This class represents name global secondary index
    """

    class Meta:  # pylint: disable=too-few-public-methods
        read_capacity_units = 1
        write_capacity_units = 1
        # All attributes are projected
        projection = AllProjection()

    name = UnicodeAttribute(hash_key=True)
    updated_at = UTCDateTimeAttributeISO(range_key=True)


class LabelIndex(GlobalSecondaryIndex):
    """
    This class represents label global secondary index
    """

    class Meta:  # pylint: disable=too-few-public-methods
        read_capacity_units = 1
        write_capacity_units = 1
        # All attributes are projected
        projection = AllProjection()

    label = UnicodeAttribute(hash_key=True)
    updated_at = UTCDateTimeAttributeISO(range_key=True)


class CorrelationIdIndex(GlobalSecondaryIndex):
    """
    This class represents correlation_id global secondary index
    """

    class Meta:  # pylint: disable=too-few-public-methods
        read_capacity_units = 1
        write_capacity_units = 1
        # All attributes are projected
        projection = AllProjection()

    correlation_id = UnicodeAttribute(hash_key=True)
    version = UnicodeAttribute(range_key=True)


class VersionIndex(GlobalSecondaryIndex):
    """
    This class represents updated_at global secondary index
    """

    class Meta:  # pylint: disable=too-few-public-methods
        read_capacity_units = 1
        write_capacity_units = 1
        # All attributes are projected
        projection = AllProjection()

    version = UnicodeAttribute(hash_key=True)
    updated_at = UTCDateTimeAttributeISO(range_key=True)


class EventStore(BaseModel):
    """
    DynamoDB EventStore Model
    """

    class Meta:  # pylint: disable=too-few-public-methods
        table_name = config.DYNAMODB_TABLE_NAME
        region = config.DYNAMODB_REGION
        host = config.DYNAMODB_HOST
        billing_mode = PAY_PER_REQUEST_BILLING_MODE

    uuid = UnicodeAttribute(hash_key=True, default=lambda: str(uuid4()))
    version = UnicodeAttribute(range_key=True, default="v_0")
    version_index = VersionIndex()
    spec_version = NumberAttribute(default=config.SPEC_VERSION, null=False)
    correlation_id = UnicodeAttribute()
    correlation_id_index = CorrelationIdIndex()
    parent_id = UnicodeAttribute(null=True)
    data_id = UnicodeAttribute(null=True)
    name = UnicodeAttribute()
    name_index = NameIndex()
    label = UnicodeAttribute()
    label_index = LabelIndex()
    name_data_id = UnicodeAttribute()
    name_data_id_index = NameDataIdIndex()
    status = UnicodeAttribute(default=StatusEnum.PENDING)
    message = UnicodeAttribute(null=True)
    created_at = UTCDateTimeAttributeISO(default=lambda: datetime.now(timezone.utc))
    updated_at = UTCDateTimeAttributeISO(default=lambda: datetime.now(timezone.utc))
    kind = UnicodeAttribute()
    resource_config = JSONAttribute()
    allow_multiple = BooleanAttribute(default=True, null=False)
    enqueue_on_previous_error = BooleanAttribute(default=False, null=True)
    ignore_on_previous_active = BooleanAttribute(default=False, null=True)
    queue_ids = ListAttribute(default=[])
    payload_file_name = UnicodeAttribute()
    additional_infos = JSONAttribute(null=True)

    def to_dict(self):
        dict_model = super().to_dict().copy()
        del dict_model["name_data_id"]
        return dict_model
