from datetime import datetime, timezone
from pynamodb.attributes import Attribute, STRING


class UTCDateTimeAttributeISO(Attribute[datetime]):
    """
    An attribute for storing a UTC Datetime
    """

    attr_type = STRING

    def serialize(self, value):
        """
        Takes a datetime object and returns a string
        """
        if value.tzinfo is None:
            value = value.replace(tzinfo=timezone.utc)
        fmt = value.astimezone(timezone.utc).isoformat()
        return fmt

    def deserialize(self, value):
        """
        Takes a UTC datetime string and returns a datetime object
        """
        return self._fast_parse_utc_date_string(value)

    @staticmethod
    def _fast_parse_utc_date_string(date_string: str) -> datetime:
        # Method to quickly parse strings formatted with '%Y-%m-%dT%H:%M:%S.%f+00:00'.
        # This is ~5.8x faster than using strptime and 38x faster than dateutil.parser.parse.
        _int = int  # Hack to prevent global lookups of int, speeds up the function ~10%
        try:
            if (
                len(date_string) != 32  # pylint: disable=too-many-boolean-expressions
                or date_string[4] != "-"
                or date_string[7] != "-"
                or date_string[10] != "T"
                or date_string[13] != ":"
                or date_string[16] != ":"
                or date_string[19] != "."
                or date_string[26:32] != "+00:00"
            ):
                raise ValueError("Datetime string '{}' does not match iso format")
            return datetime(
                _int(date_string[0:4]),
                _int(date_string[5:7]),
                _int(date_string[8:10]),
                _int(date_string[11:13]),
                _int(date_string[14:16]),
                _int(date_string[17:19]),
                _int(date_string[20:26]),
                timezone.utc,
            )
        except (TypeError, ValueError) as exc:
            raise ValueError("Datetime string '{}' does not match iso format") from exc
