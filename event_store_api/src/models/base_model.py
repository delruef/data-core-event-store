from datetime import datetime, timezone
import orjson

# from pynamodb.constants import DATETIME_FORMAT
from pynamodb.models import Model
from pynamodb.attributes import MapAttribute


class BaseModel(Model):
    def to_json(self):
        return orjson.dumps(self.to_dict()).decode()

    def to_dict(self):
        ret_dict = {}
        for name, attr in self.attribute_values.items():
            ret_dict[name] = self._attr2obj(attr)

        return ret_dict

    def _attr2obj(self, attr):
        # compare with list class. It is not ListAttribute.
        if isinstance(attr, list):
            _list = []
            for item in attr:
                _list.append(self._attr2obj(item))
            return _list

        if isinstance(attr, MapAttribute):
            _dict = {}
            for key, val in attr.attribute_values.items():
                _dict[key] = self._attr2obj(val)
            return _dict

        if isinstance(attr, datetime):
            if attr.tzinfo is None:
                attr = attr.replace(tzinfo=timezone.utc)
            return attr.astimezone(timezone.utc).isoformat()
            # return attr.astimezone(timezone.utc).strftime(DATETIME_FORMAT)

        return attr
