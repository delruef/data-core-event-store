image: hashicorp/terraform:1.0.0

definitions:
  services:
    dynamodb:
      image: amazon/dynamodb-local
      memory: 2048
      env:
        AWS_DEFAULT_REGION: eu-west-1
        AWS_ACCESS_KEY_ID: ""
        AWS_SECRET_ACCESS_KEY: ""
  steps:
    lint_and_test: &step-lint-and-test
      step:
        image: python:3.8
        size: 2x
        services:
          - dynamodb
        script:
          - apt-get update
          - pip install poetry
          - python3 -m venv venv
          - source venv/bin/activate
          # event_store_api
          - cd event_store_api
          - export ENV=testing
          - poetry install
          - pylint src
          - pylint -d duplicate-code tests
          - flake8 src tests
          - export AWS_ACCESS_KEY_ID=''
          - export AWS_SECRET_ACCESS_KEY=''
          - export ENV=testing
          - export DYNAMODB_TABLE_NAME=EventStore
          - export DYNAMODB_HOST=http://localhost:8000
          - export DYNAMODB_REGION=$AWS_DEFAULT_REGION
          # - pytest
          # event_store_client
          - cd ../event_store_client
          - poetry install
          - pylint event_store_client
          - flake8 event_store_client
    tfplanmajor: &step-major-version-tfplan
      name: Terraform plan
      artifacts:
        - terraform/major_version_modules/.out/**
    
    tfplanminor: &step-minor-version-tfplan
      name: Terraform plan
      artifacts:
        - terraform/minor_version_modules/.out/**

    tfapply: &step-tfapply
      name: Terraform apply
      trigger: manual

    tfplandestroy: &step-tfplandestroy
      name: Terraform plan destroy

    tfdestroy: &step-tfdestroy
      name: Terraform destroy
      trigger: manual

pipelines:
  default:
    - *step-lint-and-test
  tags:
    # deploy version 2.0
    v2.0:
      - *step-lint-and-test
      - parallel:
          - step:
              image: python:3.8
              name: dev code deploy
              script:
                - export TF_VAR_aws_id=$TF_AWS_ID_DEV
                - bash event_store_api/.bin/deploy.sh dev v-2-0
          - step:
              image: python:3.8
              name: int code deploy
              script:
                - export TF_VAR_aws_id=$TF_AWS_ID_INT
                - bash event_store_api/.bin/deploy.sh int v-2-0
          - step:
              image: python:3.8
              name: ppd code deploy
              trigger: manual
              script:
                - export TF_VAR_aws_id=$TF_AWS_ID_PPD
                - bash event_store_api/.bin/deploy.sh ppd v-2-0
          - step:
              image: python:3.8
              name: prd code deploy
              trigger: manual
              script:
                - export TF_VAR_aws_id=$TF_AWS_ID_PRD
                - bash event_store_api/.bin/deploy.sh prd v-2-0

  custom:
    # Development environment
    1.0.DEV_MAJOR_PLAN_APPLY:
      - step:
          <<: *step-major-version-tfplan
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_DEV
            - sh .bin/deploy.sh plan dev major_version_modules v-2
      - step:
          <<: *step-tfapply
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_DEV
            - sh .bin/deploy.sh apply dev major_version_modules v-2
    # 1.1.DEV_MAJOR_DESTROY:
    #  - step:
    #      <<: *step-tfplandestroy
    #      script:
    #        - export TF_VAR_aws_id=$TF_AWS_ID_DEV
    #        - sh .bin/deploy.sh plandestroy dev major_version_modules v-2
    #
    #  - step:
    #      <<: *step-tfdestroy
    #      script:
    #        - export TF_VAR_aws_id=$TF_AWS_ID_DEV
    #        - sh .bin/deploy.sh destroy dev major_version_modules v-2
    1.2.DEV_MINOR_INITIALIZE:
      - step:
          <<: *step-minor-version-tfplan
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_DEV
            - sh .bin/deploy.sh plan dev minor_version_modules v-2-0 -target=module.code_s3_bucket

      - step:
          <<: *step-tfapply
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_DEV
            - sh .bin/deploy.sh apply dev minor_version_modules v-2-0

      - step:
          image: python:3.8
          name: dev upload on bucket
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_DEV
            - bash event_store_api/.bin/upload_file.sh dev v-2-0
    1.3.DEV_MINOR_PLAN_APPLY:
      - step:
          <<: *step-minor-version-tfplan
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_DEV
            - sh .bin/deploy.sh plan dev minor_version_modules v-2-0
      - step:
          <<: *step-tfapply
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_DEV
            - sh .bin/deploy.sh apply dev minor_version_modules v-2-0

    # 1.4.DEV_MINOR_DESTROY:
    #   - step:
    #       <<: *step-tfplandestroy
    #       script:
    #         - export TF_VAR_aws_id=$TF_AWS_ID_DEV
    #         - sh .bin/deploy.sh plandestroy dev minor_version_modules v-2-0
    #
    #   - step:
    #       <<: *step-tfdestroy
    #       script:
    #         - export TF_VAR_aws_id=$TF_AWS_ID_DEV
    #         - sh .bin/deploy.sh destroy dev minor_version_modules v-2-0

    # Integration environment
    2.0.INT_MAJOR_PLAN_APPLY:
      - step:
          <<: *step-major-version-tfplan
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_INT
            - sh .bin/deploy.sh plan int major_version_modules v-2
      - step:
          <<: *step-tfapply
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_INT
            - sh .bin/deploy.sh apply int major_version_modules v-2
    # 2.1.INT_MAJOR_DESTROY:
    #  - step:
    #      <<: *step-tfplandestroy
    #      script:
    #        - export TF_VAR_aws_id=$TF_AWS_ID_INT
    #        - sh .bin/deploy.sh plandestroy int major_version_modules v-2
    #
    #  - step:
    #      <<: *step-tfdestroy
    #      script:
    #        - export TF_VAR_aws_id=$TF_AWS_ID_INT
    #        - sh .bin/deploy.sh destroy int major_version_modules v-2
    2.2.INT_MINOR_INITIALIZE:
      - step:
          <<: *step-minor-version-tfplan
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_INT
            - sh .bin/deploy.sh plan int minor_version_modules v-2-0 -target=module.code_s3_bucket

      - step:
          <<: *step-tfapply
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_INT
            - sh .bin/deploy.sh apply int minor_version_modules v-2-0

      - step:
          image: python:3.8
          name: int upload on bucket
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_INT
            - bash event_store_api/.bin/upload_file.sh int v-2-0
    2.3.INT_MINOR_PLAN_APPLY:
      - step:
          <<: *step-minor-version-tfplan
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_INT
            - sh .bin/deploy.sh plan int minor_version_modules v-2-0
      - step:
          <<: *step-tfapply
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_INT
            - sh .bin/deploy.sh apply int minor_version_modules v-2-0

    # 2.4.INT_MINOR_DESTROY:
    #   - step:
    #       <<: *step-tfplandestroy
    #       script:
    #         - export TF_VAR_aws_id=$TF_AWS_ID_INT
    #         - sh .bin/deploy.sh plandestroy int minor_version_modules v-2-0
    #
    #   - step:
    #       <<: *step-tfdestroy
    #       script:
    #         - export TF_VAR_aws_id=$TF_AWS_ID_INT
    #         - sh .bin/deploy.sh destroy int minor_version_modules v-2-0

    # Preproduction environment
    3.0.PPD_MAJOR_PLAN_APPLY:
      - step:
          <<: *step-major-version-tfplan
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_PPD
            - sh .bin/deploy.sh plan ppd major_version_modules v-2
      - step:
          <<: *step-tfapply
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_PPD
            - sh .bin/deploy.sh apply ppd major_version_modules v-2
    # 3.1.PPD_MAJOR_DESTROY:
    #  - step:
    #      <<: *step-tfplandestroy
    #      script:
    #        - export TF_VAR_aws_id=$TF_AWS_ID_PPD
    #        - sh .bin/deploy.sh plandestroy ppd major_version_modules v-2
    #
    #  - step:
    #      <<: *step-tfdestroy
    #      script:
    #        - export TF_VAR_aws_id=$TF_AWS_ID_PPD
    #        - sh .bin/deploy.sh destroy ppd major_version_modules v-2
    3.2.PPD_MINOR_INITIALIZE:
      - step:
          <<: *step-minor-version-tfplan
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_PPD
            - sh .bin/deploy.sh plan ppd minor_version_modules v-2-0 -target=module.code_s3_bucket

      - step:
          <<: *step-tfapply
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_PPD
            - sh .bin/deploy.sh apply ppd minor_version_modules v-2-0

      - step:
          image: python:3.8
          name: ppd upload on bucket
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_PPD
            - bash event_store_api/.bin/upload_file.sh ppd v-2-0
    3.3.PPD_MINOR_PLAN_APPLY:
      - step:
          <<: *step-minor-version-tfplan
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_PPD
            - sh .bin/deploy.sh plan ppd minor_version_modules v-2-0
      - step:
          <<: *step-tfapply
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_PPD
            - sh .bin/deploy.sh apply ppd minor_version_modules v-2-0

    # 3.4.PPD_MINOR_DESTROY:
    #   - step:
    #       <<: *step-tfplandestroy
    #       script:
    #         - export TF_VAR_aws_id=$TF_AWS_ID_PPD
    #         - sh .bin/deploy.sh plandestroy ppd minor_version_modules v-2-0
    #
    #   - step:
    #       <<: *step-tfdestroy
    #       script:
    #         - export TF_VAR_aws_id=$TF_AWS_ID_PPD
    #         - sh .bin/deploy.sh destroy ppd minor_version_modules v-2-0

    # Production environment
    4.0.PRD_MAJOR_PLAN_APPLY:
      - step:
          <<: *step-major-version-tfplan
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_PRD
            - sh .bin/deploy.sh plan prd major_version_modules v-2
      - step:
          <<: *step-tfapply
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_PRD
            - sh .bin/deploy.sh apply prd major_version_modules v-2
    # 4.1.PRD_MAJOR_DESTROY:
    #  - step:
    #      <<: *step-tfplandestroy
    #      script:
    #        - export TF_VAR_aws_id=$TF_AWS_ID_PRD
    #        - sh .bin/deploy.sh plandestroy prd major_version_modules v-2
    #
    #  - step:
    #      <<: *step-tfdestroy
    #      script:
    #        - export TF_VAR_aws_id=$TF_AWS_ID_PRD
    #        - sh .bin/deploy.sh destroy prd major_version_modules v-2
    4.2.PRD_MINOR_INITIALIZE:
      - step:
          <<: *step-minor-version-tfplan
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_PRD
            - sh .bin/deploy.sh plan prd minor_version_modules v-2-0 -target=module.code_s3_bucket

      - step:
          <<: *step-tfapply
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_PRD
            - sh .bin/deploy.sh apply prd minor_version_modules v-2-0

      - step:
          image: python:3.8
          name: prd upload on bucket
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_PRD
            - bash event_store_api/.bin/upload_file.sh prd v-2-0
    4.3.PRD_MINOR_PLAN_APPLY:
      - step:
          <<: *step-minor-version-tfplan
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_PRD
            - sh .bin/deploy.sh plan prd minor_version_modules v-2-0
      - step:
          <<: *step-tfapply
          script:
            - export TF_VAR_aws_id=$TF_AWS_ID_PRD
            - sh .bin/deploy.sh apply prd minor_version_modules v-2-0
    # 4.4.PRD_MINOR_DESTROY:
    #   - step:
    #       <<: *step-tfplandestroy
    #       script:
    #         - export TF_VAR_aws_id=$TF_AWS_ID_PRD
    #         - sh .bin/deploy.sh plandestroy prd minor_version_modules v-2-0
    #
    #   - step:
    #       <<: *step-tfdestroy
    #       script:
    #         - export TF_VAR_aws_id=$TF_AWS_ID_PRD
    #         - sh .bin/deploy.sh destroy prd minor_version_modules v-2-0