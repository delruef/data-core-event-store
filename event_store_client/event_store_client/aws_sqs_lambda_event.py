import json

from pydash import get

from .aws_base_lambda_event import AwsBaseLambdaEvent
from .helpers import EventStoreException, ErrorsEnum, Status


class AwsSqsLambdaEvent(AwsBaseLambdaEvent):
    def __init__(self, lambda_event, lambda_context, api_url, api_key):
        super().__init__(
            lambda_event,
            lambda_context,
            api_url,
            api_key,
        )
        self._body = self._get_body()

    def _get_body(self):
        json_body = get(
            self._lambda_event,
            "Records.0.body",
        )
        return json.loads(json_body) if json_body else {}

    def _get_event_uuid(self):
        return get(
            self._body, "MessageAttributes.event_id.Value"
        ) or self._lambda_event.get("eventId")

    def _get_default_name(self):
        method = get(self._body, "MessageAttributes.methode.Value").lower()
        return f"{self._lambda_context.function_name}-{method}"

    def _get_default_parent_id(self):
        return get(self._body, "MessageAttributes.event_parent_id.Value")

    def _get_default_data_id(self):
        json_path_parameters = get(self._body, "MessageAttributes.pathParameters.Value")
        if json_path_parameters:
            path_parameters = json.loads(json_path_parameters)
            return get(path_parameters, "id", get(path_parameters, "uuid"))

        return None

    async def _init(
        self,
        *,
        name=None,
        allow_multiple=None,
        enqueue_on_previous_error=None,
        ignore_on_previous_active=None,
        label=None,
        status=Status.PENDING,
        parent_id=None,
        data_id=None,
        event_uuid=None,
        additional_infos=None,
    ):
        name = name or self._get_default_name()
        parent_id = parent_id or get(
            self._body, "MessageAttributes.event_parent_id.Value"
        )
        data_id = data_id if data_id is not None else self._get_default_data_id()
        enqueue_on_previous_error = (
            enqueue_on_previous_error
            if enqueue_on_previous_error is not None
            else data_id is not None
            and get(self._body, "MessageAttributes.methode.Value") == "PATCH"
        )

        update_event_uuid = self._get_event_uuid()
        event_uuid = event_uuid or update_event_uuid
        correlation_id_original = get(
            self._body,
            "MessageAttributes.correlation_id.Value",
        )

        self._event = await self._create_or_update_event(
            name=name,
            allow_multiple=allow_multiple,
            enqueue_on_previous_error=enqueue_on_previous_error,
            ignore_on_previous_active=ignore_on_previous_active,
            label=label,
            status=status,
            parent_id=parent_id,
            data_id=data_id,
            event_uuid=event_uuid,
            is_update=bool(update_event_uuid),
            additional_infos=additional_infos,
        )
        self._correlation_id = self.event.get("correlation_id", self.event.get("uuid"))

        if correlation_id_original and correlation_id_original != self._correlation_id:
            raise EventStoreException(json.dumps(ErrorsEnum.CORRELATION_ID_MISMATCH))


async def create_lambda_event(
    lambda_event,
    lambda_context,
    api_url,
    api_key,
    *,
    name=None,
    parent_id=None,
    data_id=None,
    allow_multiple=None,
    enqueue_on_previous_error=None,
    ignore_on_previous_active=None,
    label=None,
    status=Status.PENDING,
    event_uuid=None,
    additional_infos=None,
):
    event_service = AwsSqsLambdaEvent(
        lambda_event,
        lambda_context,
        api_url,
        api_key,
    )
    await event_service._init(  # pylint: disable=protected-access
        name=name,
        allow_multiple=allow_multiple,
        enqueue_on_previous_error=enqueue_on_previous_error,
        ignore_on_previous_active=ignore_on_previous_active,
        label=label,
        status=status,
        parent_id=parent_id,
        data_id=data_id,
        event_uuid=event_uuid,
        additional_infos=additional_infos,
    )
    return event_service
