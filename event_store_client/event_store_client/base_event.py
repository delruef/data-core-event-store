from .service import EventService
from .helpers import Status, EventStoreEventQueuedException


class BaseEvent:  # pylint: disable=too-many-instance-attributes
    def __init__(self, api_url, api_key):
        self.event_service = EventService(api_url, api_key)

        self._event = None
        self._correlation_id = None

    @property
    def correlation_id(self):
        return self._correlation_id

    @property
    def event(self):
        return self._event

    async def edit(self, **payload):
        self._event = await self.event_service.edit_event(
            self.event.get("uuid"), **payload
        )

    async def success(self, message=None):
        self._event = await self.event_service.success(self.event.get("uuid"), message)

    async def fail(self, message=None):
        self._event = await self.event_service.fail(self.event.get("uuid"), message)

    async def enqueue(self, message=None):
        self._event = await self.event_service.enqueue(self.event.get("uuid"), message)
        if self._event.get("status") == Status.QUEUED:
            raise EventStoreEventQueuedException(self._event)
