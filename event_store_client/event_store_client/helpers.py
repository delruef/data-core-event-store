from enum import Enum


class EventStoreException(Exception):
    pass


class EventStoreEventQueuedException(Exception):
    def __init__(self, event) -> None:
        super().__init__()
        self.event = event

    def __str__(self):
        event_uuid = self.event.get("uuid")

        return f"event {event_uuid} queued"


class EventStoreEventIgnoredException(Exception):
    def __init__(self, event) -> None:
        super().__init__()
        self.event = event

    def __str__(self):
        event_uuid = self.event.get("uuid")

        return f"event {event_uuid} ignored"


class Kinds(str, Enum):
    AWS_LAMBDA = "aws_lambda"
    API_CALL = "api_call"


class Status(str, Enum):
    ACCEPTED = "accepted"
    QUEUED = "queued"
    PENDING = "pending"
    ERROR = "error"
    FINISHED = "finished"
    IGNORED = "ignored"


class ErrorsEnum(str, Enum):
    CORRELATION_ID_MISMATCH = {
        "code": "event_store_client_correlation_id_mismatch",
        "messasge": """The correlation id in header
            is different than the one associated to the event""",
    }
