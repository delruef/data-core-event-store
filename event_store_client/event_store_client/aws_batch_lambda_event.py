from .aws_base_lambda_event import AwsBaseLambdaEvent
from .helpers import Status


class AwsBatchLambdaEvent(AwsBaseLambdaEvent):
    async def _init(
        self,
        *,
        name=None,
        allow_multiple=None,
        enqueue_on_previous_error=None,
        ignore_on_previous_active=None,
        label=None,
        status=Status.PENDING,
        event_uuid=None,
        additional_infos=None,
        data_id=None
    ):

        update_event_uuid = self._lambda_event.get("eventId")
        event_uuid = event_uuid or update_event_uuid
        self._event = await self._create_or_update_event(
            name=name,
            allow_multiple=allow_multiple,
            enqueue_on_previous_error=enqueue_on_previous_error,
            ignore_on_previous_active=ignore_on_previous_active,
            label=label,
            status=status,
            event_uuid=event_uuid,
            is_update=bool(update_event_uuid),
            additional_infos=additional_infos,
            data_id=data_id
        )
        self._correlation_id = self.event.get("uuid")


async def create_lambda_event(
    lambda_event,
    lambda_context,
    api_url,
    api_key,
    *,
    name=None,
    allow_multiple=None,
    enqueue_on_previous_error=None,
    ignore_on_previous_active=True,
    label=None,
    status=Status.PENDING,
    event_uuid=None,
    additional_infos=None,
    data_id=None
):
    event_service = AwsBatchLambdaEvent(lambda_event, lambda_context, api_url, api_key)
    await event_service._init(  # pylint: disable=protected-access
        name=name,
        allow_multiple=allow_multiple,
        enqueue_on_previous_error=enqueue_on_previous_error,
        ignore_on_previous_active=ignore_on_previous_active,
        label=label,
        status=status,
        event_uuid=event_uuid,
        additional_infos=additional_infos,
        data_id=data_id
    )
    return event_service
