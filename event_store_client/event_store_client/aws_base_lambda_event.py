from .helpers import (
    EventStoreException,
    EventStoreEventQueuedException,
    EventStoreEventIgnoredException,
    Kinds,
    Status,
)
from .base_event import BaseEvent


class AwsBaseLambdaEvent(BaseEvent):  # pylint: disable=too-many-instance-attributes
    def __init__(self, lambda_event, lambda_context, api_url, api_key):
        super().__init__(api_url, api_key)

        self._lambda_event = lambda_event
        self._lambda_context = lambda_context

    async def _create_or_update_event(
        self,
        *,
        name=None,
        allow_multiple=None,
        enqueue_on_previous_error=None,
        ignore_on_previous_active=None,
        label=None,
        status=Status.PENDING,
        parent_id=None,
        data_id=None,
        event_uuid=None,
        is_update=False,
        additional_infos=None
    ):
        allow_multiple = allow_multiple if allow_multiple is not None else not data_id
        name = name or self._lambda_context.function_name
        label = label or name

        json_payload = {
            **({"uuid": event_uuid} if event_uuid and not is_update else {}),
            "parent_id": parent_id,
            "data_id": data_id,
            "name": name,
            "label": label,
            **({"status": status} if event_uuid and is_update else {}),
            "allow_multiple": allow_multiple,
            "enqueue_on_previous_error": enqueue_on_previous_error,
            "ignore_on_previous_active": ignore_on_previous_active,
            "kind": Kinds.AWS_LAMBDA,
            "resource_config": {
                "function_name": self._lambda_context.invoked_function_arn,
            },
            "payload": self._lambda_event,
            "additional_infos": additional_infos,
        }

        try:
            event = (
                await self.event_service.update_event(event_uuid, json_payload)
                if event_uuid and is_update
                else await self.event_service.create_event(json_payload)
            )

            if event.get("status") == Status.QUEUED:
                raise EventStoreEventQueuedException(event)

            if event.get("status") == Status.IGNORED:
                raise EventStoreEventIgnoredException(event)

            return event

        except EventStoreEventQueuedException as exc:
            raise exc
        except EventStoreEventIgnoredException as exc:
            raise exc

        except Exception as exc:
            raise EventStoreException(str(exc)) from exc
