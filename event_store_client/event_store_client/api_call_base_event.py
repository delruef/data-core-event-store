from .helpers import (
    Status,
    EventStoreEventQueuedException,
    EventStoreEventIgnoredException,
    EventStoreException,
    Kinds,
)
from .base_event import BaseEvent


class ApiCallBaseEvent(BaseEvent):  # pylint: disable=too-many-instance-attributes
    async def _create_or_update_event(
        self,
        name,
        url,
        method,
        *,
        headers=None,
        query_params=None,
        body=None,
        allow_multiple=None,
        enqueue_on_previous_error=None,
        ignore_on_previous_active=None,
        label=None,
        status=Status.PENDING,
        parent_id=None,
        data_id=None,
        event_uuid=None,
        is_update=False,
        additional_infos=None,
    ):  # pylint: disable=too-many-locals
        allow_multiple = allow_multiple if allow_multiple is not None else not data_id
        enqueue_on_previous_error = (
            enqueue_on_previous_error
            if enqueue_on_previous_error is not None
            else data_id is not None and method.upper() == "PATCH"
        )
        label = label or name

        json_payload = {
            **({"uuid": event_uuid} if event_uuid and not is_update else {}),
            "parent_id": parent_id,
            "data_id": data_id,
            "name": name,
            "label": label,
            **({"status": status} if event_uuid and is_update else {}),
            "allow_multiple": allow_multiple,
            "enqueue_on_previous_error": enqueue_on_previous_error,
            "ignore_on_previous_active": ignore_on_previous_active,
            "kind": Kinds.API_CALL,
            "resource_config": {
                "url": url,
                "method": method.upper(),
            },
            "payload": {
                "headers": headers,
                "query_params": query_params,
                "body": body,
            },
            "additional_infos": additional_infos,
        }

        try:
            event = (
                await self.event_service.update_event(event_uuid, json_payload)
                if event_uuid and is_update
                else await self.event_service.create_event(json_payload)
            )

            if event.get("status") == Status.QUEUED:
                raise EventStoreEventQueuedException(event)

            if event.get("status") == Status.IGNORED:
                raise EventStoreEventIgnoredException(event)

            return event

        except EventStoreEventQueuedException as exc:
            raise exc

        except EventStoreEventIgnoredException as exc:
            raise exc

        except Exception as exc:
            raise EventStoreException(str(exc)) from exc

    async def _init(
        self,
        name,
        url,
        method,
        *,
        headers=None,
        query_params=None,
        body=None,
        allow_multiple=None,
        enqueue_on_previous_error=None,
        ignore_on_previous_active=None,
        label=None,
        status=Status.PENDING,
        parent_id=None,
        data_id=None,
        event_uuid=None,
        additional_infos=None,
    ):  # pylint: disable=too-many-locals

        lowercase_headers = (
            {k.lower(): v for k, v in headers.items()} if headers else {}
        )
        update_event_uuid = (
            lowercase_headers.get("x-event-id") if lowercase_headers else None
        )
        event_uuid = event_uuid or update_event_uuid

        self._event = await self._create_or_update_event(
            name,
            url,
            method,
            headers=headers,
            query_params=query_params,
            body=body,
            allow_multiple=allow_multiple,
            enqueue_on_previous_error=enqueue_on_previous_error,
            ignore_on_previous_active=ignore_on_previous_active,
            label=label,
            status=status,
            parent_id=parent_id,
            data_id=data_id,
            event_uuid=event_uuid,
            is_update=bool(update_event_uuid),
            additional_infos=additional_infos,
        )
        self._correlation_id = self.event.get("correlation_id", self.event.get("uuid"))


async def create_api_call_event(
    api_url,
    api_key,
    name,
    url,
    method,
    *,
    headers=None,
    query_params=None,
    body=None,
    parent_id=None,
    data_id=None,
    allow_multiple=None,
    enqueue_on_previous_error=None,
    ignore_on_previous_active=None,
    label=None,
    status=Status.PENDING,
    event_uuid=None,
    additional_infos=None,
):  # pylint: disable=too-many-locals
    event_service = ApiCallBaseEvent(api_url, api_key)
    await event_service._init(  # pylint: disable=protected-access
        name,
        url,
        method,
        headers=headers,
        query_params=query_params,
        body=body,
        allow_multiple=allow_multiple,
        enqueue_on_previous_error=enqueue_on_previous_error,
        ignore_on_previous_active=ignore_on_previous_active,
        label=label,
        status=status,
        parent_id=parent_id,
        data_id=data_id,
        event_uuid=event_uuid,
        additional_infos=additional_infos,
    )
    return event_service
