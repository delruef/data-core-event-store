import json

import aiohttp

from .helpers import EventStoreException, Status


class EventService:
    def __init__(self, api_url, api_key):
        self._session = None
        self._api_url = api_url
        self._api_key = api_key

    @property
    def session(self):
        return self._session

    @staticmethod
    async def get_formatted_response(response):
        if response.content_type == "application/json":
            return await response.json()

        return await response.text()

    async def request_path(self, path, method="GET", **kwargs):
        response_body = None
        url = f"{self._api_url}{path}"

        try:
            await self.open_session()

            response = await self._session.request(
                method, url, raise_for_status=False, **kwargs
            )

            response_body = await EventService.get_formatted_response(response)
            response.raise_for_status()

            return response_body

        except aiohttp.ClientResponseError as exc:
            error = json.dumps(
                {
                    "statusCode": exc.status,
                    "message": exc.message,
                    "data": response_body,
                    "url": url,
                    "method": method,
                }
            )
            raise EventStoreException(error) from exc
        finally:
            await self.close_session()

    async def open_session(self):
        if not self._session or self._session.closed:
            self._session: aiohttp.ClientSession = aiohttp.ClientSession(
                headers={
                    "x-api-key": self._api_key,
                    "Accept": "application/json",
                }
            )

    async def close_session(self):
        if self._session and not self._session.closed:
            await self._session.close()

    async def get_event(self, event_uuid):
        return await self.request_path(f"/events/{event_uuid}")

    async def get_many_events(self, **params):
        return await self.request_path(
            "/events",
            params=params,
        )

    async def create_event(self, payload):
        return await self.request_path(
            "/events",
            "POST",
            json=payload,
        )

    async def update_event(self, event_uuid, payload):
        return await self.request_path(
            f"/events/{event_uuid}",
            "PUT",
            json=payload,
        )

    async def edit_event(self, event_uuid, **payload):

        if not event_uuid:
            raise EventStoreException("Cannot update event without uuid")

        return await self.request_path(
            f"/events/{event_uuid}",
            "PATCH",
            json=payload,
        )

    async def edit_status(self, event_uuid, status, message):
        message = (
            json.dumps(message)
            if isinstance(message, dict)
            else str(message)
            if message
            else None
        )

        return await self.edit_event(event_uuid, status=status, message=message)

    async def success(self, event_uuid, message=None):
        return await self.edit_status(
            event_uuid, status=Status.FINISHED, message=message
        )

    async def fail(self, event_uuid, message=None):
        return await self.edit_status(event_uuid, status=Status.ERROR, message=message)

    async def enqueue(self, event_uuid, message=None):
        return await self.edit_status(event_uuid, status=Status.QUEUED, message=message)
