from uuid import uuid4
import json

from pydash import get

from .helpers import EventStoreException, ErrorsEnum, Status
from .aws_base_lambda_event import AwsBaseLambdaEvent


class AwsHttpLambdaEvent(
    AwsBaseLambdaEvent
):  # pylint: disable=too-many-instance-attributes
    def __init__(
        self,
        lambda_event,
        lambda_context,
        api_url,
        api_key,
        *,
        create_when_get=False,
    ):
        super().__init__(
            lambda_event,
            lambda_context,
            api_url,
            api_key,
        )

        headers = self._lambda_event.get("headers")
        self._lowercase_headers = (
            {k.lower(): v for k, v in headers.items()} if headers else {}
        )
        self._should_create = (
            create_when_get or self._lambda_event.get("httpMethod") != "GET"
        )

    async def _init(
        self,
        *,
        name=None,
        allow_multiple=None,
        enqueue_on_previous_error=None,
        ignore_on_previous_active=None,
        label=None,
        status=Status.PENDING,
        parent_id=None,
        data_id=None,
        compute_data_id=True,
        event_uuid=None,
        additional_infos=None,
    ):
        correlation_id_header = (
            self._lowercase_headers.get("x-correlation-id")
            if self._lowercase_headers
            else None
        )

        parent_id = parent_id or self._get_default_parent_id()
        data_id = data_id or (self._get_default_data_id() if compute_data_id else None)
        enqueue_on_previous_error = (
            enqueue_on_previous_error
            if enqueue_on_previous_error is not None
            else data_id is not None and self._lambda_event.get("httpMethod") == "PATCH"
        )

        update_event_uuid = self._get_event_uuid()
        event_uuid = event_uuid or update_event_uuid

        if not self._should_create:
            self._correlation_id = correlation_id_header or str(uuid4())
            return

        self._event = await self._create_or_update_event(
            name=name,
            allow_multiple=allow_multiple,
            enqueue_on_previous_error=enqueue_on_previous_error,
            ignore_on_previous_active=ignore_on_previous_active,
            label=label,
            status=status,
            parent_id=parent_id,
            data_id=data_id,
            event_uuid=event_uuid,
            is_update=bool(update_event_uuid),
            additional_infos=additional_infos,
        )
        self._correlation_id = self.event.get("correlation_id", self.event.get("uuid"))

        if correlation_id_header and correlation_id_header != self._correlation_id:
            raise EventStoreException(json.dumps(ErrorsEnum.CORRELATION_ID_MISMATCH))

    def _get_event_uuid(self):
        return (
            self._lowercase_headers.get("x-event-id")
            if self._lowercase_headers
            else None
        ) or self._lambda_event.get("eventId")

    def _get_default_parent_id(self):
        return (
            self._lowercase_headers.get("x-event-parent-id")
            if self._lowercase_headers
            else None
        )

    def _get_default_data_id(self):
        path_parameters = self._lambda_event.get("pathParameters")
        return get(path_parameters, "id", get(path_parameters, "uuid"))

    async def success(self, message=None):
        if self._should_create:
            await super().success(message)

    async def fail(self, message=None):
        if self._should_create:
            await super().fail(message)

    async def enqueue(self, message=None):
        if self._should_create:
            await super().enqueue(message)


async def create_lambda_event(
    lambda_event,
    lambda_context,
    api_url,
    api_key,
    *,
    name=None,
    parent_id=None,
    data_id=None,
    compute_data_id=True,
    create_when_get=False,
    allow_multiple=None,
    enqueue_on_previous_error=None,
    ignore_on_previous_active=None,
    label=None,
    status=Status.PENDING,
    event_uuid=None,
    additional_infos=None,
):  # pylint: disable=too-many-locals
    event_service = AwsHttpLambdaEvent(
        lambda_event,
        lambda_context,
        api_url,
        api_key,
        create_when_get=create_when_get,
    )
    await event_service._init(  # pylint: disable=protected-access
        name=name,
        allow_multiple=allow_multiple,
        enqueue_on_previous_error=enqueue_on_previous_error,
        ignore_on_previous_active=ignore_on_previous_active,
        label=label,
        status=status,
        parent_id=parent_id,
        data_id=data_id,
        compute_data_id=compute_data_id,
        event_uuid=event_uuid,
        additional_infos=additional_infos,
    )
    return event_service
