# -*- coding: utf-8 -*-
from setuptools import setup

packages = ["event_store_client"]

package_data = {"": ["*"]}

install_requires = ["aiohttp>=3.7.4,<4.0.0", "pydash>=5.0.0,<6.0.0"]

setup_kwargs = {
    "name": "event-store-client",
    "version": "2.0.0",
    "description": "",
    "long_description": None,
    "author": "Ciro ALABRESE",
    "author_email": "ciro.alabrese@external.terega.fr",
    "maintainer": None,
    "maintainer_email": None,
    "url": None,
    "packages": packages,
    "package_data": package_data,
    "install_requires": install_requires,
    "python_requires": ">=3.8,<4.0",
}


setup(**setup_kwargs)
