# Terega event store client

## Utilisation en locale

### Installation

- [Installer un environnement virtuel](https://docs.python.org/fr/3/library/venv.html#creating-virtual-environments)
- activer l'environnement virtuel
- une fois le venv activé
  
  `pip install poetry`

  `poetry install`

### Génération et mise à jour du setup.py

- Lancer la commande

  `poetry2setup > setup.py`
