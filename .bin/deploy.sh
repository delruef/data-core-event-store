#!/bin/sh

export TF_VAR_runtime=app
export TF_VAR_aws_env=$2
export FOLDER=$3
export TF_VAR_version_name=$4
export TF_VAR_las_aws_id=$TF_AWS_ID_LAS
export TF_VAR_aws_account=$TF_VAR_aws_env

command=$1

shift 4
plan_args=$1

usage() {
  echo "Usage:"
  echo "$0 command env version"
  echo "command: (string) plan|apply|destroy"
  echo "env: (string) Terraform environment, see env files"
  echo "version: (string) App version, v-0-1|v-0-2|v-1-1..."
}

tfrun() {
  tfcmd=$1
  plan_supplementary_command=$2

  # Display some messages
  case "$tfcmd" in
  plan)
    echo "# If you plan to apply modifications, review CAREFULLY the Terraform plan result below"
    echo "# before launching the corresponding APPLY pipeline..."
    ;;
  apply)
    echo "# This is the APPLY phase."
    echo "# This pipeline must be launched ONLY if a PLAN has been launched before (otherwise no PLAN to apply)."
    ;;
  plandestroy)
    echo "# This is the PLAN DESTROY phase."
    ;;
  destroy)
    echo "# !!! WARNING !!! WARNING !!! WARNING !!! WARNING !!!"
    echo "# This pipeline will DESTROY Network Infrastructure for $env Environment."
    echo "# !!! WARNING !!! WARNING !!! WARNING !!! WARNING !!!"
    ;;
  esac
  echo

  terraform init -backend-config="external_id=$TF_VAR_external_id" \
    -backend-config="key=$TF_VAR_code_app/$TF_VAR_aws_env-$TF_VAR_version_name.tfstate" \
    -backend-config="role_arn=arn:aws:iam::$TF_VAR_aws_id:role/rol-$TF_VAR_code_soc-$TF_VAR_runtime-$TF_VAR_code_app-$TF_VAR_aws_account-deploy" \
    -backend-config="region=$TF_VAR_aws_region" \
    -backend-config="bucket=s3-$TF_VAR_code_soc-core-deploy-projects-$TF_VAR_aws_account-tfstate" \
    -var-file=$envfile

  terraform validate

  mkdir -p .out
  planfile=".out/$env-${BITBUCKET_REPO_SLUG}_$BITBUCKET_COMMIT.plan"

  # Run the requested command
  case "$tfcmd" in
  plan)
    terraform plan -out "$planfile" -var-file="$envfile" $plan_supplementary_command
    ;;
  apply)
    terraform apply "$planfile"
    ;;
  plandestroy)
    terraform plan -destroy -out "$planfile" -var-file="$envfile"
    ;;
  destroy)
    terraform destroy -force -var-file="$envfile"
    ;;
  esac
}

cd terraform/$FOLDER || exit

if [ "$command" != "plan" ] && [ "$command" != "apply" ] && [ "$command" != "destroy" ] && [ "$command" != "plandestroy" ]; then
  usage
  exit 1
fi

if [ -z "$TF_VAR_aws_env" ]; then
  usage
  exit 1
fi

if [ -z "$TF_VAR_version_name" ]; then
  usage
  exit 1
fi

# We are currently located in the Terraform directory due to a previous cd command
envfile="../envs/$TF_VAR_aws_account.tfvars"
if [ ! -f "$envfile" ]; then
  echo "# This Terraform environment is missing its backend or env file: $TF_VAR_aws_account"
  echo "# Tested files: $envfile"
  exit 1
fi

echo "#"
echo "# Summary"
echo "# -------"
echo "#"
echo "# Command: $command"
echo "# Account: $TF_VAR_aws_account"
echo "# Environment : $TF_VAR_aws_env"
echo "#"
echo

tfrun "$command" "$plan_args"
exit $?
